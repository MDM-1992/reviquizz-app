package controller;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import metier.Classe;
import metier.User;

import java.awt.Dialog;
import java.awt.JobAttributes.DialogType;
import java.io.IOException;
import java.sql.SQLException;
import controller.MainAppAdmin;
import dao.ClasseDAO;
import dao.UserDAO;


/**
 * @author Anaya Kassamaly
  * @notes Ce controller d�stin� aux administrateurs est utilis� pour la
  * gestion des Classes
  * @functionality Affiche le detail des classes / 
  * Suppression, ajout et modification des classes .
  * 
 */
public class ListeClasseController {
    @FXML
    private TableView<Classe> classeTable;
    @FXML
    private TableColumn<Classe, String> classeCol;
   
    @FXML
    private Label NomClasseLabel;
    
    @FXML
    private Button addButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Button editButton;
    
    @FXML
    private MainAppConnect mainAppConnect;
    
    private User user;
    
    @FXML
    private void initialize() {
        // Initialize la table etablissement avec les colonnes.
    
    	
    	updateTable();
    	
    	classeCol.setCellValueFactory(cellData -> cellData.getValue().getLibelleP());
        
        
        // renitialise les details de personne
        showClasseDetails(null);
        
        
        //cherche les changements de s�lection et affiche les d�tails de la personne quand ils sont modifi�s.
     classeTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showClasseDetails(newValue));
    }

    
   
    

    private void updateTable() {
		// TODO Auto-generated method stub
    	classeTable.setItems(getClasseData());
	}


	/**
     * Les donn�es sont une observable list de classe.
     */
    private ObservableList<Classe> ClasseData = FXCollections.observableArrayList();

    /**
     * Renvoie les donn�es sous forme d' observable liste classe. 
     * @return
     */
    public ObservableList<Classe> getClasseData() {
    	ClasseDAO classedao = new ClasseDAO();
    	try {
			ClasseData = classedao.findAll() ;
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return ClasseData;
    }
    
    /**
     * Est appel� par le main application pour renvoyer une r�f�rence.
     *
     * @param mainApp
     */
    public void setMainAppConnect(MainAppConnect mainAppAdmin) {
        this.mainAppConnect = mainAppAdmin;

        // Add observable list data to the table
      
    }
    /**
     * Remplit tous les champs de texte pour afficher des d�tails sur la classe.
     * Si la classe sp�cifi�e est nulle, tous les champs de texte sont effac�s.
     *
     * @param classe the classe or null
     */
    private void showClasseDetails(Classe classe) {
        if (classe != null) {
            // Fill the labels with info from the person object.
            NomClasseLabel.setText(classe.getLibelle());

        } else {
            // Person is null, remove all the text.
            NomClasseLabel.setText("");

            
        }
    }
    /**
     * Appell� quand l'admin clique sur le boutton delete.
     */
    @FXML
    private void handleDeleteClasse() {
    	Classe classe =  classeTable.getSelectionModel().getSelectedItem();
    	ClasseDAO classed = new ClasseDAO();
    	int nbligne =0;
    	if (classe != null) {
    		  
    	try {
			 nbligne = classed.delete(classe.getIdClasse());
			updateTable() ;
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
    	}else {
            // Alerte si aucune classe n'est s�lectionn�. 
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainAppConnect.getPrimaryStage());
            alert.setTitle("Pas de selection");
            alert.setHeaderText("Pas de classe s�lectionn�e");
            alert.setContentText("Veuillez selectionner une classe.");
            alert.showAndWait();
        }
       if ( nbligne == 1)
       {
    	   
    	   Alert alert = new Alert(AlertType.INFORMATION);
    
           alert.initOwner(mainAppConnect.getPrimaryStage());
           alert.setTitle("Suppression ");
           alert.setHeaderText("La classe que vous avez s�l�ctionn�e � �t� supprim�e.");
           alert.setContentText("Vous vennez de supprimer une Classe.");
           alert.showAndWait();
        } 
    }
    

	/**
     * Appel� quand l'administrateur clique sur le boutton nouveau.
     * Ouverture de dialogue pour appliquer un nouvelle etablissement.
     */
    @FXML
    private void handleNewClasse() {
        Classe tempClasse = null;
        boolean okClicked = showClasseEditDialog(tempClasse);
        if (okClicked) {
        	
        	updateTable() ;
        }
    }

    /**
     * Appel� quand l'administrateur clique sur le boutton modifier. 
     * Ouverture de dialogue pour modifier les details d'un �tablissement.
     */
    @FXML
    private void handleEditClasse() {
    	Classe selectedClasse = classeTable.getSelectionModel().getSelectedItem();
        System.out.println(selectedClasse);
        if (selectedClasse != null) {
            boolean okClicked = showClasseEditDialog(selectedClasse);
            if (okClicked) {
            
                showClasseDetails(selectedClasse);
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.initOwner(mainAppConnect.getPrimaryStage());
                alert.setTitle("Modification");
                alert.setHeaderText("Modification d'ue Classe");
                alert.setContentText("Vous venez d'effectuer une modification");
                
                alert.showAndWait();
            }

        } else {
            // Nothing selected.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainAppConnect.getPrimaryStage());
            alert.setTitle("Aucune s�lection");
            alert.setHeaderText("Pas de classe s�lectionn�e");
            alert.setContentText("Veuillez s�lectionner une classe.");

            alert.showAndWait();
        }
    }
    public boolean showClasseEditDialog(Classe classe) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getClassLoader().getResource("vue/ClasseEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Classe");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.mainAppConnect.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            ClasseEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setClasse(classe);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user =user ;
	}
    }