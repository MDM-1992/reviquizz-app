package controller;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import metier.Etablissement;
import metier.User;

import java.awt.Dialog;
import java.awt.JobAttributes.DialogType;
import java.io.IOException;
import java.sql.SQLException;
import controller.MainAppAdmin;
import dao.EtablissementDAO;
import dao.UserDAO;


/**
 * @author Anaya Kassamaly
  * @notes Ce controller d�stin� aux administrateurs est utilis� pour la
  * gestion des etablissements
  * @functionality Affiche le detail des etablissement / 
  * Suppression, ajout et modification des etablissements .
  * 
 */
public class ListeEtablissementController {
    @FXML
    private TableView<Etablissement> etablissementTable;
    @FXML
    private TableColumn<Etablissement, String> etablissementCol;
   
    @FXML
    private Label NomEtabLabel;
    
    @FXML
    private Label codePostalLabel;
    
    @FXML
    private Label adresseLabel;
    
    @FXML
    private Label villeLabel;
    
    @FXML
    private Button addButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Button editButton;
    
    @FXML
    private MainAppConnect mainAppConnect;
    
    private User user;
    
    @FXML
    private void initialize() {
        // Initialize la table etablissement avec les colonnes.
    	System.out.println(getEtablissementData());
    	
    	updateTable();
    	
    	etablissementCol.setCellValueFactory(cellData -> cellData.getValue().getLibelleP());
        
        
        // renitialise les details de personne
        showEtablissementDetails(null);
        
        
        //cherche les changements de s�lection et affiche les d�tails de la personne quand ils sont modifi�s.
     etablissementTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showEtablissementDetails(newValue));
    }

    
   
    

    private void updateTable() {
		// TODO Auto-generated method stub
    	etablissementTable.setItems(getEtablissementData());
	}


	/**
     * Les donn�es sont une observable list d'Etablissement.
     */
    private ObservableList<Etablissement> EtablissementData = FXCollections.observableArrayList();

    /**
     * Renvoie les donn�es sous forme d' observable liste Etablissement. 
     * @return
     */
    public ObservableList<Etablissement> getEtablissementData() {
    	EtablissementDAO etabdao = new EtablissementDAO();
    	try {
			EtablissementData = etabdao.findAll() ;
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return EtablissementData;
    }
    
    /**
     * Est appel� par le main application pour renvoyer une r�f�rence.
     *
     * @param mainApp
     */
    public void setMainAppConnect(MainAppConnect mainAppAdmin) {
        this.mainAppConnect = mainAppAdmin;

        // Add observable list data to the table
      
    }
    /**
     * Remplit tous les champs de texte pour afficher des d�tails sur l'�tablissement.
     * Si l'�tablissement sp�cifi�e est nulle, tous les champs de texte sont effac�s.
     *
     * @param etablissement the etablissement or null
     */
    private void showEtablissementDetails(Etablissement etablissement) {
        if (etablissement != null) {
            // Fill the labels with info from the person object.
            NomEtabLabel.setText(etablissement.getLibelle());
            codePostalLabel.setText(Integer.toString(etablissement.getCodePostal()));
            adresseLabel.setText(etablissement.getadresse());
            villeLabel.setText(etablissement.getville());
            

        } else {
            // Person is null, remove all the text.
            NomEtabLabel.setText("");
            codePostalLabel.setText("");
            adresseLabel.setText("");
            villeLabel.setText("");
            
        }
    }
    /**
     * Appell� quand l'admin clique sur le boutton delete.
     */
    @FXML
    private void handleDeleteEtablissement() {
    	Etablissement etablissement =  etablissementTable.getSelectionModel().getSelectedItem();
    	EtablissementDAO etablissementd = new EtablissementDAO();
    	int nbligne =0;
    	if (etablissement != null) {
    		  
    	try {
			 nbligne = etablissementd.delete(etablissement.getIdEtablissement());
			updateTable() ;
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
    	}else {
            // Alerte si aucun etablissement n'est s�lectionn�. 
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainAppConnect.getPrimaryStage());
            alert.setTitle("Pas de selection");
            alert.setHeaderText("Pas d'etablissement s�lectionn�");
            alert.setContentText("Veuillez selectionner un etablissement.");
            alert.showAndWait();
        }
       if ( nbligne == 1)
       {
    	   
    	   Alert alert = new Alert(AlertType.INFORMATION);
    
           alert.initOwner(mainAppConnect.getPrimaryStage());
           alert.setTitle("Suppression ");
           alert.setHeaderText("L'etablissement que vous avez s�l�ctionn� � �t� supprim�.");
           alert.setContentText("Vous vennez de supprimer un Etablissement.");
           alert.showAndWait();
        } 
    }
    

	/**
     * Appel� quand l'administrateur clique sur le boutton nouveau.
     * Ouverture de dialogue pour appliquer un nouvelle etablissement.
     */
    @FXML
    private void handleNewEtablissement() {
        Etablissement tempEtablissement = null;
        boolean okClicked = showEtablissementEditDialog(tempEtablissement);
        if (okClicked) {
        	
        	updateTable() ;
        }
    }

    /**
     * Appel� quand l'administrateur clique sur le boutton modifier. 
     * Ouverture de dialogue pour modifier les details d'un �tablissement.
     */
    @FXML
    private void handleEditEtablissement() {
        Etablissement selectedEtablissement = etablissementTable.getSelectionModel().getSelectedItem();
        System.out.println(selectedEtablissement);
        if (selectedEtablissement != null) {
            boolean okClicked = showEtablissementEditDialog(selectedEtablissement);
            if (okClicked) {
            
                showEtablissementDetails(selectedEtablissement);
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.initOwner(mainAppConnect.getPrimaryStage());
                alert.setTitle("Modification");
                alert.setHeaderText("Modification d'un Etablissement");
                alert.setContentText("Vous venez d'effectuer une modification");
                
                alert.showAndWait();
            }

        } else {
            // Nothing selected.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainAppConnect.getPrimaryStage());
            alert.setTitle("Aucune s�lection");
            alert.setHeaderText("Pas d'�tablissement s�lectionn�");
            alert.setContentText("Veuillez s�lectionner un �tablissement.");

            alert.showAndWait();
        }
    }
    public boolean showEtablissementEditDialog(Etablissement etablissement) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getClassLoader().getResource("vue/EtablissementEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Etablissement");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.mainAppConnect.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            EtablissementEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setEtablissement(etablissement);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user =user ;
	}
    }