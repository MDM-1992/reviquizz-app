package controller;

import java.io.IOException;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metier.User;
	
public class MainAppAdmin extends Application {

    private Stage primaryStage;
    private BorderPane adminMenu;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("reviquizz-app");

     initAdminMenu();

    }
    
    /**
     * Initialize  l'adminMenu.
     */
    public void initAdminMenu() {
        try {
            // Charge l'adminMenu du fichier fxml.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/adminMenu.fxml"));
            adminMenu = (BorderPane) loader.load();
            
            //  Affiche la scene contenant l'adminMenu
            Scene scene = new Scene(adminMenu);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
   
// ======================================================ListUSER============================================================

    /**
     * The data as an observable list of Persons.
     */
    private ObservableList<User> userData = FXCollections.observableArrayList();

    /**
     * Retourne les donn�es en tant qu'observable list de personne
     * @return
     */
    public ObservableList<User> getUserData() {
        return userData;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>UserEditDialog>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /**
     * Opens a dialog to edit details for the specified person. If the user
     * clicks OK, the changes are saved into the provided person object and true
     * is returned.
     *
     * @param person the person object to be edited
     * @return true if the user clicked OK, false otherwise.
     */
   
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>FinUserEditDialog>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    //===================================================finListUSER===================================================
    /**
     *retourne le main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
