package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import dao.Connect;
import dao.QuestionDAO;
import dao.QuestionnaireDAO;
import dao.ReponseDAO;
import javafx.beans.property.IntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import metier.Classe;
import metier.Matiere;
import metier.Niveau;
import metier.Question;
import metier.Questionnaire;
import metier.Reponse;
import metier.User;

public class ListeQuizzController {

		int cptQuestionTotal = 1 ; //Compteur pour le nombre de questions total
		int cptQuestionEnCours = 1; //Compteur pour la question en cours
		int nbQuestionsTotal = 20; //Compteur pour le nombre de questions max
		//Init de l'objet Questionnaire 
		Questionnaire questionnaireEnCours = new Questionnaire();
		Questionnaire recupQuestionnaire;
		//Init de la question en cours
		Question questionEnCours = new Question();
		Question recupQuestion;
		//Init des 4 questions
		Reponse reponseA = new Reponse();
		Reponse reponseB = new Reponse();
		Reponse reponseC = new Reponse();
		Reponse reponseD = new Reponse();
		Reponse recupReponse;
		
			@FXML
		    private TableView<Questionnaire> tview_quizz;

		    @FXML
		    private TableColumn<Questionnaire, String> col_libelle;


		    @FXML
		    private TableColumn<Questionnaire, Integer> col_nbQuestions;

		    @FXML
		    private TableColumn<Questionnaire, Integer> col_id;

		    @FXML
		    private ChoiceBox<Matiere> box_matiere;

		    @FXML
		    private ChoiceBox<Classe> box_classe;

		    @FXML
		    private TextField text_libQuizz;

		    @FXML
		    private Button btn_nouveauQuizz;

		    @FXML
		    private Button btn_valider;

		    @FXML
		    private Button btn_precedent;

		    @FXML
		    private Button btn_suivant;

		    @FXML
		    private Label lab_question;

		    @FXML
		    private TextField text_libQuestion;

		    @FXML
		    private TextField text_reponseA;

		    @FXML
		    private TextField text_reponseB;

		    @FXML
		    private TextField text_reponseC;

		    @FXML
		    private TextField text_reponseD;

		    @FXML
		    private CheckBox check_A;

		    @FXML
		    private CheckBox check_B;

		    @FXML
		    private CheckBox check_C;

		    @FXML
		    private CheckBox check_D;

		    @FXML
		    private Button btn_ajoutQuestion;

		    @FXML
		    private Button btn_suppr;
		    
		    // Reference a l'application principale.
		    private MainAppConnect mainAppConnect;

		    @FXML
		    private void initialize() throws ClassNotFoundException, SQLException {
		    	remplissageCombo();
		    	questionnaireEnCours.initTable();
		    	questionEnCours.initTable();
		    	cptQuestionEnCours = 1;
		    	cptQuestionTotal = 1;
		    	text_libQuizz.setText("");
		    	text_libQuestion.clear();
		    	btn_precedent.setDisable(true);
		    	btn_valider.setText("Creer le quizz");
		    	btn_suivant.setDisable(true);
		    	btn_ajoutQuestion.setDisable(false);
		    	lab_question.setText("Question num "+cptQuestionEnCours+" ("+cptQuestionEnCours+"/"+cptQuestionTotal+")");
		    	text_reponseA.clear();
		    	text_reponseB.clear();
		    	text_reponseC.clear();
		    	text_reponseD.clear();
		    	check_A.setSelected(false);
		    	check_B.setSelected(false);
		    	check_C.setSelected(false);
		    	check_D.setSelected(false);	
		        // Initialise la table Quizz
		        updateTable();
		    	text_libQuestion.setText("");
		    	col_libelle.setCellValueFactory(cellData -> cellData.getValue().getLibelleP());
		    	col_nbQuestions.setCellValueFactory(cellData -> cellData.getValue().getNbQuestionsP().asObject());
		    	col_id.setCellValueFactory(cellData -> cellData.getValue().getIdQuestionnaireP().asObject());
		         
		         
		        // renitialise les details de personne
		        try {
					showQuestionnaireDetails(null);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		         
		         
		         //cherche les changements de slection et affiche les dtails de la personne quand ils sont modifis.
		       tview_quizz.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
				try {
					showQuestionnaireDetails(newValue);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		    }

		    private ObservableList<Questionnaire> questionnaireData = FXCollections.observableArrayList();
		    
			   public ObservableList<Questionnaire> getQuestionnaireData() {
			        return questionnaireData;
			    }
			   
			private void updateTable(){
				try {
		    		QuestionnaireDAO questionnairedao = new QuestionnaireDAO() ;
		    		
		    		ObservableList<Questionnaire> liste = questionnairedao.findAll();
		    		 tview_quizz.setItems(liste);
		    		
		    		} catch (SQLException e) {
		    			// TODO Auto-generated catch block
		    			e.printStackTrace();
		    		} catch (ClassNotFoundException e) {
		    			// TODO Auto-generated catch block
		    			e.printStackTrace();
		    		}

			}

		    private void showQuestionnaireDetails(Questionnaire questionnaire) throws SQLException {
		    	
		        if (questionnaire != null) {	
		        	clear();
		            // Remplir les label avec les informations de l'objet personne
		        	questionnaireEnCours = new Questionnaire();
		        	QuestionnaireDAO recupQuestionnaireDAO = new QuestionnaireDAO();
		        	questionnaireEnCours = recupQuestionnaireDAO.recupQuestionnaire(questionnaire.getIdQuestionnaire());      	
		            text_libQuizz.setText(questionnaire.getLibelle());
		            questionnaireEnCours.initTable();
		            
		    		String req = "SELECT idQuestion, libelleQuestion, numQuestion FROM question WHERE question.idQuestionnaire = ?" ;
		    		Connection cnx = Connect.getInstance().getConnection() ;
		    		PreparedStatement stm = cnx.prepareStatement(req);

		    		stm.setInt(1, questionnaireEnCours.getIdQuestionnaire());
		    		
		    		int id;
		    	    String libelle;
		    	    int numQuestion;
		    	    int i = 0;

		    	    ResultSet jeu = stm.executeQuery();
		    	    while(jeu.next()){
		    	    	id = jeu.getInt("idQuestion");
		    	        libelle = jeu.getString("libelleQuestion");
		    	        numQuestion = jeu.getInt("numQuestion");
		    	        	 
		    	        Question question = new Question();
		    	        question.setLibelle(libelle);
		    	        question.setIdQuestion(id);
		    	        question.setNumQuestion(numQuestion);
		    	        System.out.println(question.getLibelle());
		    	        questionnaireEnCours.setList(i, question);
		    	        i++;
		    	        cptQuestionTotal = cptQuestionTotal +1;
		    	    }
		    	    jeu.close();
		          	stm.close();
		            cnx.close();

		            for (int j=0;j<i;j++) {
			    		req = "SELECT idReponse, libelle, valeur FROM reponse WHERE reponse.idQuestion = ?" ;
			    		cnx = Connect.getInstance().getConnection() ;
			    		stm = cnx.prepareStatement(req);

			    		stm.setInt(1, questionnaireEnCours.getList(j).getIdQuestion());
			    		
			    	    int valeur;
			    	    Question provisoire = new Question();
			    	    provisoire = questionnaireEnCours.getList(j);
			    	    provisoire.initTable();
			    	    jeu = stm.executeQuery();
			    	    int k = 0;
			    	    while(jeu.next()){
			    	    	id = jeu.getInt("idReponse");
			    	        libelle = jeu.getString("libelle");
			    	        valeur = jeu.getInt("valeur");
			    	        	 
			    	        Reponse reponse = new Reponse();
			    	        reponse.setLibelle(libelle);
			    	        reponse.setIdReponse(id);
			    	        if(valeur == 1) {
			    	        	reponse.setValeur(true);
			    	        }else{
			    	        	reponse.setValeur(false);
			    	        }
			    	        provisoire.setList(k, reponse);
			    	        k++;
			    	    }
			    	    questionnaireEnCours.setList(j, provisoire);
		            }
		            jeu.close();
		          	stm.close();
		            cnx.close();

		            recupQuestion = questionnaireEnCours.getList(0);
		            text_libQuestion.setText(recupQuestion.getLibelle());
		            text_reponseA.setText(recupQuestion.getList(0).getLibelle());   //--> remplir avec la premiere question
		            check_A.setSelected(recupQuestion.getList(0).getValeur());
		            text_reponseB.setText(recupQuestion.getList(1).getLibelle()); //--> remplir avec la deuxieme question
		            check_B.setSelected(recupQuestion.getList(1).getValeur());
		            text_reponseC.setText(recupQuestion.getList(2).getLibelle()); //--> remplir avec la troisieme question
		            check_C.setSelected(recupQuestion.getList(2).getValeur());
		            text_reponseD.setText(recupQuestion.getList(3).getLibelle()); //--> remplir avec la quatrieme question
		            check_D.setSelected(recupQuestion.getList(3).getValeur());
		            //Besoin de mettre la coche pour la bonne reponse !!!
		            //choix du niveau scolaire dans la choice box !!!
		            
		            lab_question.setText("Question num 1 (1/"+ questionnaire.getNbQuestions()+")");
		            cptQuestionEnCours = 1;
		            cptQuestionTotal = questionnaire.getNbQuestions();
		            btn_valider.setDisable(true);
		            btn_suivant.setDisable(false);
		            btn_ajoutQuestion.setDisable(true);
		            btn_suppr.setDisable(false);
		            
		        } else {
		            // Si Questionnaire = null alors champs vide
		            clear();
		            text_libQuizz.clear();
		        }
		    }
		    

		    
	    @FXML
	    void ajoutQuestion(ActionEvent event) {
	    	System.out.println(box_matiere.getSelectionModel().getSelectedIndex());
	    	enregistrerQuestion();
	    	cptQuestionTotal++;
	    	if (cptQuestionTotal>5) {
	    		btn_valider.setDisable(false);
	    	}
	    	if(cptQuestionTotal == nbQuestionsTotal) {
	    		btn_ajoutQuestion.setDisable(true);
	    	}
	    	cptQuestionEnCours = cptQuestionTotal;
	    	text_libQuestion.clear();
	    	btn_precedent.setDisable(false);
	    	btn_suivant.setDisable(true);
	    	lab_question.setText("Question num "+cptQuestionEnCours+" ("+cptQuestionEnCours+"/"+cptQuestionTotal+")");
	    	text_reponseA.clear();
	    	text_reponseB.clear();
	    	text_reponseC.clear();
	    	text_reponseD.clear();
	    	check_A.setSelected(false);
	    	check_B.setSelected(false);
	    	check_C.setSelected(false);
	    	check_D.setSelected(false);
	    	
	    }

	    @FXML
	    void newQuizz(ActionEvent event) {	    	
	    	questionnaireEnCours = new Questionnaire();
	    	questionEnCours = new Question();
	    	questionnaireEnCours.initTable();
	    	questionEnCours.initTable();
	    	cptQuestionEnCours = 1;
	    	cptQuestionTotal = 1;
	    	text_libQuizz.clear();
	    	text_libQuestion.clear();
	    	btn_precedent.setDisable(true);
	    	btn_suivant.setDisable(true);
	    	btn_ajoutQuestion.setDisable(false);
	    	lab_question.setText("Question num "+cptQuestionEnCours+" ("+cptQuestionEnCours+"/"+cptQuestionTotal+")");
	    	text_libQuizz.setText("");
	    	btn_valider.setText("Creer le quizz");
	    	text_reponseA.clear();
	    	text_reponseB.clear();
	    	text_reponseC.clear();
	    	text_reponseD.clear();
	    	check_A.setSelected(false);
	    	check_B.setSelected(false);
	    	check_C.setSelected(false);
	    	check_D.setSelected(false);	
	    	btn_valider.setDisable(false);
	    	btn_suppr.setDisable(true);
	    }
	    
	    @FXML
	    void questionPrecedente(ActionEvent event) {
	    	if(enregistrerQuestion() == true) {
	    		cptQuestionEnCours--;
		    	if (cptQuestionEnCours == 1) {
		    		btn_precedent.setDisable(true);
		    	}
		    	if (cptQuestionEnCours < 20) {
		    		btn_suivant.setDisable(false);
		    	}
		    	lab_question.setText("Question num "+cptQuestionEnCours+" ("+cptQuestionEnCours+"/"+cptQuestionTotal+")");
		    	Question questionProvisoire = new Question();
		    	questionProvisoire = questionnaireEnCours.getList(cptQuestionEnCours-1);
		    	clear();
		    	//utilisation de l'array list pour remplissage des objets scene builders
		    	text_libQuestion.setText(questionProvisoire.getLibelle());
		    	Reponse reponseProvisoire = new Reponse();
		    	reponseProvisoire = questionProvisoire.getList(0);
		    	text_reponseA.setText(reponseProvisoire.getLibelle());
		    	check_A.setSelected(reponseProvisoire.getValeur());
		    	reponseProvisoire = questionProvisoire.getList(1);
		    	text_reponseB.setText(reponseProvisoire.getLibelle());
		    	check_B.setSelected(reponseProvisoire.getValeur());
		    	reponseProvisoire = questionProvisoire.getList(2);
		    	text_reponseC.setText(reponseProvisoire.getLibelle());
		    	check_C.setSelected(reponseProvisoire.getValeur());
		    	reponseProvisoire = questionProvisoire.getList(3);
		    	text_reponseD.setText(reponseProvisoire.getLibelle());
		       	check_D.setSelected(reponseProvisoire.getValeur());
	    	}
	    }
	    

	    @FXML
	    void questionSuivante(ActionEvent event) {
	    	if (enregistrerQuestion() == true) {
	    		cptQuestionEnCours++;
		    	if (cptQuestionEnCours == cptQuestionTotal) {
		    		btn_suivant.setDisable(true);
		    	}
		    	if (cptQuestionEnCours > 1) {
		    		btn_precedent.setDisable(false);
		    	}
		    	lab_question.setText("Question num "+cptQuestionEnCours+" ("+cptQuestionEnCours+"/"+cptQuestionTotal+")");
		    	Question questionProvisoire = new Question();
		    	questionProvisoire = questionnaireEnCours.getList(cptQuestionEnCours-1);
		    	clear();
		    	//utilisation de l'array list pour remplissage des objets scene builders
		    	text_libQuestion.setText(questionProvisoire.getLibelle());
		    	Reponse reponseProvisoire = new Reponse();
		    	reponseProvisoire = questionProvisoire.getList(0);
		    	text_reponseA.setText(reponseProvisoire.getLibelle());
		    	check_A.setSelected(reponseProvisoire.getValeur());
		    	reponseProvisoire = questionProvisoire.getList(1);
		    	text_reponseB.setText(reponseProvisoire.getLibelle());
		    	check_B.setSelected(reponseProvisoire.getValeur());
		    	reponseProvisoire = questionProvisoire.getList(2);
		    	text_reponseC.setText(reponseProvisoire.getLibelle());
		    	check_C.setSelected(reponseProvisoire.getValeur());
		    	reponseProvisoire = questionProvisoire.getList(3);
		    	text_reponseD.setText(reponseProvisoire.getLibelle());
		    	check_D.setSelected(reponseProvisoire.getValeur());
	    	}
	    }
	    
	    //Partie de gestion pour n'avoir qu'une seule bonne reponse --> bas� sur le cochage d'un des champs check_box
	    @FXML
	    void eventCocheA(ActionEvent event) {
    	   	check_B.setSelected(false);
    	   	check_C.setSelected(false);
    	   	check_D.setSelected(false);
	    }
	    @FXML
	    void eventCocheB(ActionEvent event) {
	    	check_A.setSelected(false);
    	   	check_C.setSelected(false);
    	   	check_D.setSelected(false);
	    }
	    @FXML
	    void eventCocheC(ActionEvent event) {
	    	check_A.setSelected(false);
    	   	check_B.setSelected(false);
    	   	check_D.setSelected(false);
	    }
	    @FXML
	    void eventCocheD(ActionEvent event) {
	    	check_A.setSelected(false);
    	   	check_B.setSelected(false);
    	   	check_C.setSelected(false);
	    }

	    @FXML
	    void supprimeQuizz(ActionEvent event) {
	    	Questionnaire questionnaire =   tview_quizz.getSelectionModel().getSelectedItem();
	    	QuestionnaireDAO questionnaired = new QuestionnaireDAO();
	    	int nbligne =0;
	    	if (questionnaire != null) {
	    	try {
				 nbligne = questionnaired.delete(questionnaire.getIdQuestionnaire());
				 updateTable();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	}
	    }

	    @FXML
	    void validation(ActionEvent event) {
	    	enregistrerQuestion();
	    	questionnaireEnCours.setNbQuestions(cptQuestionEnCours);
	    	questionnaireEnCours.setLibelle(text_libQuizz.getText());
	    	QuestionnaireDAO questionnaireEnCoursDao = new QuestionnaireDAO();
	    	try {
				questionnaireEnCours.setIdQuestionnaire(questionnaireEnCoursDao.insert(questionnaireEnCours));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	for(int i = 0;i<cptQuestionTotal;i++) { 
	    		questionEnCours = questionnaireEnCours.getList(i);
	    		questionEnCours.setQuestionnaire(questionnaireEnCours);
	    		QuestionDAO questionEnCoursDAO = new QuestionDAO();
	    		try {
					questionEnCours.setIdQuestion(questionEnCoursDAO.insert(questionEnCours));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					
					e.printStackTrace();
				}
	    		for(int j=0;j<4;j++) {
	    			Reponse reponseEnCours = new Reponse();
	    			reponseEnCours = questionEnCours.getList(j);
	    			reponseEnCours.setQuestion(questionEnCours);
	    			ReponseDAO reponseDAO = new ReponseDAO();
	    			try {
						reponseDAO.insert(reponseEnCours);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    		}
	    	}
	    	System.out.println("Ajout base de donnee efectuee");
	    	clear();
	    	text_libQuizz.clear();
	    	updateTable();
	    	//Enregistrement base de donnees des instances de questionnaire, question et reponses en parcourant les observableList
	    	//Actualisation table
	    }
	    
	    boolean enregistrerQuestion() {
	    	boolean enregistrementReussi = false;
	    	//On remplit l'instance Question en cours d'�dition
	    	questionEnCours = new Question();
	    	questionEnCours.setQuestionnaire(questionnaireEnCours);
	    	questionEnCours.setNumQuestion(cptQuestionEnCours);
	    	questionEnCours.setLibelle(text_libQuestion.getText());
	    	questionEnCours.initTable();
	    	//On remplit les instances reponse en cours d'�dition 
	    	reponseA = new Reponse();
	    	reponseA.setLibelle(text_reponseA.getText());
	    	reponseA.setQuestion(questionEnCours);
	    	reponseA.setValeur(check_A.isSelected());
	    	reponseB = new Reponse();
	    	reponseB.setLibelle(text_reponseB.getText());
	    	reponseB.setQuestion(questionEnCours);
	    	reponseB.setValeur(check_B.isSelected());
	    	reponseC = new Reponse();
	    	reponseC.setLibelle(text_reponseC.getText());
	    	reponseC.setQuestion(questionEnCours);
	    	reponseC.setValeur(check_C.isSelected());
	    	reponseD = new Reponse();
	    	reponseD.setLibelle(text_reponseD.getText());
	    	reponseD.setQuestion(questionEnCours);
	    	reponseD.setValeur(check_D.isSelected());
	    	//On verifie que la question est donnee
	    	if (text_libQuestion.getText() != null) {
	    		//On verifie que tout les champs des reponses sont remplis
		    	if (reponseA.getLibelle() == "" || reponseB.getLibelle() == "" || reponseC.getLibelle() == "" || reponseD.getLibelle() == "") {
		    		/*alert.initOwner(mainAppAdmin.getPrimaryStage());
		            alert.setTitle("Information manquante");
		            alert.setHeaderText("Vous n'avez pas toutes les reponses requises");
		            alert.setContentText("Veuillez remplir les quatres parties reponse .");
		            alert.showAndWait();*/
		    	}
		    	//Tout les champs sont remplis
		    	else { 
		    		//On verifie qu'au moins l'une des reponses soit cochee
		    		if (check_A.isSelected() == true || check_B.isSelected() == true || check_C.isSelected() == true || check_D.isSelected() == true)  {		    	    	
		    	    	//Ajout des reponse dans la question		    			
		    	    	questionEnCours.setList(0, reponseA);
		    	    	questionEnCours.setList(1, reponseB);
		    	    	questionEnCours.setList(2, reponseC);
		    	    	questionEnCours.setList(3, reponseD);
		    	    	
		    	    	//Ajout de la question dans le questionnaire
		    	    	questionnaireEnCours.setList(cptQuestionEnCours-1,questionEnCours);
		    	    	enregistrementReussi = true;
		    		}		    	
		    	}
	    	}  
	    		return enregistrementReussi;
	    }
	    
	    public void remplissageCombo() throws ClassNotFoundException, SQLException {
	    	//Remplissage ChoiceBox Matiere
	    	ObservableList<Matiere> MatiereList = FXCollections.observableArrayList();
	        // constitution d'une commande base sur une requte SQL 
	        // en vue d'tre excute sur une connexion donne     
	        String req = "SELECT idMatiere, libelle FROM matiere";
	        Connection cnx = Connect.getInstance().getConnection() ;
	        int idMatiere;
	        String libelle ;
	        PreparedStatement pst = cnx.prepareStatement(req);

	        ResultSet jeu = pst.executeQuery();
	        while(jeu.next()){
	       	 idMatiere = jeu.getInt("idMatiere");
	       	 libelle = jeu.getString("libelle");
	       
	       	 Matiere matiere = new Matiere();
	       	 matiere.setIdMatiere(idMatiere);
	       	 matiere.setlibelle(libelle);

	       	 MatiereList.add(matiere);
	        }
	        jeu.close();
	        pst.close();
	        cnx.close();
	        
	        box_matiere.setItems(MatiereList);
	        
	        //Remplissage ChoiceBox Classe
	        ObservableList<Classe> ClasseList = FXCollections.observableArrayList();
	        // constitution d'une commande base sur une requte SQL 
	        // en vue d'tre excute sur une connexion donne     
	         req = "SELECT id_classe,libelle FROM classe" ;
	         cnx = Connect.getInstance().getConnection() ;
	        int id_classe;
	         pst = cnx.prepareStatement(req);

	        jeu = pst.executeQuery();
	        while(jeu.next()){
	        	id_classe = jeu.getInt("id_classe");
	       	 	libelle = jeu.getString("libelle");
	       	 
	       	 	Classe classe = new Classe();
	       	 	classe.setIdClasse(id_classe);
	       		classe.setLibelle(libelle);
	       		ClasseList.add(classe);
	        }
	        jeu.close();
	        pst.close();
	        cnx.close();
	        
	        box_classe.setItems(ClasseList);
	    }
	    
	    void clear() { //permet de vider les champs correspondant aux questions et aux r�ponses
	    	text_libQuestion.clear();
    	   	text_reponseA.clear();
    	   	text_reponseB.clear();
    	   	text_reponseC.clear();
    	   	text_reponseD.clear();
    	   	check_A.setSelected(false);
    	   	check_B.setSelected(false);
    	   	check_C.setSelected(false);
    	   	check_D.setSelected(false);
	    }
	    
	   public void setMainAppConnect(MainAppConnect mainAppConnect) {
	        this.mainAppConnect = mainAppConnect;
	    }
	    

}
