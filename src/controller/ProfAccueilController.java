package controller;


import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import metier.User;

/**
 * @author Marc Missler
 * @notes Ce controller sert � afficher la page d'accueil des professeurs.
 * @functionality bouton liste utilisateur / bouton Quiz
 */
public class ProfAccueilController {
	
	private MainAppConnect mainAppConnect;
	private User user;
	
	   @FXML
	    private Button btnEleves;

	    @FXML
	    private Button btnQuiz;

    public void showListeUserProf(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/listeUserProf.fxml"));
            AnchorPane listeUser = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            mainAppConnect.getConnect().setCenter(listeUser);
            
         //  Donne au controller l'acc�s � mainAppConnect
           ListeUserProfController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
	public void setMainAppConnect(MainAppConnect mainAppAdmin) {
        this.mainAppConnect = mainAppAdmin;
        
    }
	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user =user ;
	}
}