package controller;


import java.sql.SQLException;

import dao.EtablissementDAO;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import metier.Etablissement;
import metier.User;

/**
 * Author Anaya Kassamaly
 */
public class EtablissementEditDialogController {

    @FXML
    private TextField etablissementField;
    @FXML
    private TextField codePostalField;
    @FXML
    private TextField adresseField;
    @FXML
    private TextField villeField;
    @FXML
    private Label etablissementLabel;
    @FXML
    private Label codePostalLabel;
    @FXML
    private Label adresseLabel;
    @FXML
    private Label villeLabel;
   


    private User user;
    private Stage dialogStage;
    private Etablissement etablissement ;
    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     *
     * @param person
     */
    public void setEtablissement(Etablissement etablissement) {
    	
        this.etablissement = etablissement;
        	if ( etablissement !=null) {
        etablissementField.setText(etablissement.getLibelle());
        codePostalField.setText(Integer.toString(etablissement.getCodePostal()));
        adresseField.setText(etablissement.getadresse());
        villeField.setText(etablissement.getville());
}
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
        	
        	if (etablissement == null) {
        		etablissement = new Etablissement();
            etablissement.setLibelle(etablissementField.getText());
            etablissement.setcodePostal(Integer.parseInt(codePostalField.getText()));
            etablissement.setadresse(adresseField.getText());
            etablissement.setville(villeField.getText());
            EtablissementDAO etatdao = new EtablissementDAO();
            try {
				etatdao.insert(etablissement);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
        	}else {
				
				etablissement.setLibelle(etablissementField.getText());
	            etablissement.setcodePostal(Integer.parseInt(codePostalField.getText()));
	            etablissement.setadresse(adresseField.getText());
	            etablissement.setville(villeField.getText());
	            EtablissementDAO etatdao = new EtablissementDAO();
	            try {
					etatdao.ModifEtablissement(etablissement);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
            // update
            
        	}
            okClicked = true;
            dialogStage.close();
        
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (etablissementField.getText() == null || etablissementField.getText().length() == 0) {
            errorMessage += "Etablissement non valide !";
        }
        if (codePostalField.getText() == null || codePostalField.getText().length() == 0) {
            errorMessage += "Code postal non valide (N'entrez que des chiffres)!";
        } else {
            // try to parse the postal code into an int.
            try {
                Integer.parseInt(codePostalField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Code Postal non valide !";
            }
        
        if (adresseField.getText() == null || adresseField.getText().length() == 0) {
            errorMessage += "Adresse non valide !";
        }
        if (villeField.getText() == null || villeField.getText().length() == 0) {
            errorMessage += "Ville non valide !";

        
        }

        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Donn�es invalides");
            alert.setHeaderText("Veuillez entrer les bonnes donn�es.");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user =user ;
	}
}