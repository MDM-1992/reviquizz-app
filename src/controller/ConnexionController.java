package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.xml.stream.util.EventReaderDelegate;

import application.MainApp;
import dao.UserDAO;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import metier.User;

/**
 * @author Marc Missler
 *@note Ce controller sert � renvoyer son utilisateur au moment de la connexion
 * vers les pages ou il a une habilitation
 */
public class ConnexionController {

	@FXML
	private TextField login ;
    @FXML
    private PasswordField mdpT;
	
	@FXML
	private BorderPane connect;
	private MainAppConnect mainAppConnect;
	private User user ;
	
	
//===========================================GESTION DES ROLES ================================================================
	/**
     * Renvoi l'utilisateur dans les pages qui lui son d�di�s
     */
	@FXML
public  void connexion(Event evt) throws IOException, SQLException {

		String login1 = login.getText();
		String mdp = mdpT.getText() ;
		user = 	UserDAO.UserVerif(login1, mdp) ;
		
		if (user.getRole().contentEquals("[\"ROLE_ADMIN\"]") ) {
			System.out.println("ok");
			//showAdminAccueil();
			showAdminAccueil();
			initAdminMenu();	
		}else {
			if (user.getRole().contentEquals("[\"ROLE_PROFESSEUR\"]") ) {
				System.out.println("ok");
				initProfMenu();
				showProfAccueil();
				
			}else {
				if (user.getRole().contentEquals("[\"ROLE_ELEVE\"]")) {
					
					showEleveAccueil();
				}
			}
			
			
			
		}
		}
		
		//===========================================FIN GESTION DES ROLES =======================================================
	/*	String login1 = login.getText();
		String mdp = mdpT.getText() ;
	try {
		user = 	UserDAO.UserVerif(login1, mdp) ;
		System.out.println("Bienvenue � "+ user.getAdresse() + "vous etes de type : "+ user.getRole());
		connect.getChildren().clear();*/

//	} catch (SQLException e) {
		// TODO Auto-generated catch block
	//	e.printStackTrace();
	//}


	/*if ( user.getRole() == 2){
	    /*FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("/vue/adminMenu.fxml"));
        AnchorPane profMenu = (AnchorPane) loader.load();
        connect.setCenter(profMenu);


	}

	if ( user.getRole() == 1){


	}*/


	
	
	
	//==============================================MENUS===========================================================================
	/**
     * Initialise et affiche le adminMenu.
     */
    public void initAdminMenu() {
        try {
            // Charge adminMenu du fichier fxml.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/adminMenu.fxml"));
            AnchorPane  adminMenu = (AnchorPane) loader.load();
            AdminMenuController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
           controller.setUser(user);
          MainAppConnect.getConnect().setLeft(adminMenu);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	/**
     * Initialize profMenu.
     */
    public void initProfMenu() {
        try {
            // Charge profMenu du fichier fxml.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/profMenu.fxml"));
            AnchorPane  profMenu = (AnchorPane) loader.load();
            ProfMenuController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
           controller.setUser(user);
          MainAppConnect.getConnect().setLeft(profMenu);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
  //==============================================FIN-MENUS====================================================================
   
    //+++++++++++++++++++++++++++++++++++++++++++Affichage-des-pages-D'accueil++++++++++++++++++++++++++++++++++++++++++
    public void showAdminAccueil() {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/adminAccueil.fxml"));
            AnchorPane listeUser = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(listeUser);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            AdminAccueilController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showProfAccueil() {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/profAccueil.fxml"));
            AnchorPane profAccueil = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(profAccueil);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            ProfAccueilController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showEleveAccueil() {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/eleveAccueil.fxml"));
            AnchorPane eleveAccueil = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(eleveAccueil);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            EleveAccueilController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
  //+++++++++++++++++++++++++++++++++++++++++++Fin d'affichage-des-pages-D'accueil++++++++++++++++++++++++++++++++++++++++++
// ======================================================Affichage des listes============================================================
    /**
     *  Affiche la listUser dans l'adminMenu.
     */
    public void showListeUser() {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/listeUser.fxml"));
            AnchorPane listeUser = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(listeUser);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            ListeUserController controller = loader.getController();
       
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showListeMatiere() {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/listeMatiere.fxml"));
            AnchorPane listeMatiere = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(listeMatiere);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            ListeMatiereController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showListeEtablissement() {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/listeEtablissement.fxml"));
            AnchorPane listeMatiere = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(listeMatiere);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            ListeEtablissementController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     *  Affiche la listUser dans l'adminMenu.
     */
    public void showListeUserProf() {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/listeClasse.fxml"));
            AnchorPane listeUser = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            ListeClasseController controller = loader.getController();
          
           controller.setMainAppConnect(mainAppConnect);
       
           controller.setUser(user);
            connect.setCenter(listeUser);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     *  Affiche la page d'acceuil de l'administrateur.
     */
   /* public void showAdminAccueil() {
        try {
            // Charge la page d'acceuil de l'administrateur.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/adminAccueil.fxml"));
            AnchorPane adminAccueil = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            connect.setCenter(adminAccueil);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            MainAppConnect controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

*/

	public User getUser() {
		return user;
	}




	public void setMainAppConnect(MainAppConnect mainAppConnect) {
		// TODO Auto-generated method stub
		this.mainAppConnect = mainAppConnect ;
	}


	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user = user ;
	}


}
