package controller;

import java.io.IOException;

import application.MainAppAdmin;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import metier.User;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;

/**
 * @author Marc Missler
 * @notes Ce controller sert � afficher le menu administrateur � gauche de l'application
 * @functionality bouton liste utilisateur / bouton liste mati�re / bouton liste etablissement
 */
public class AdminMenuController{

    @FXML
    private AnchorPane menu;

    @FXML
    private Button btnEtablissement;

    @FXML
    private Button btnUser;

    @FXML
    private Button btnMatiere;
    
    @FXML
    private Button btnDeconnexion;

  
	private MainAppConnect mainAppConnect;
	private User user  ;
	
	
	/**
	 *Renvoi l'utilisateur � la page de connexion lors de la d�connexion.
	 */
	public void showConnexion(ActionEvent evt) {
	       try {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainAppConnect.class.getResource("/vue/connexion.fxml")); 
        BorderPane connect  = (BorderPane) loader.load();
        
        //Supprime les scenes de AdminAccueil et AdminMenu lors de la deconnexion
        MainAppConnect.getConnect().setLeft(null);
        MainAppConnect.getConnect().setCenter(null);
      
        MainAppConnect.getConnect().setCenter(connect);
        ConnexionController controller = loader.getController();
        System.out.println(controller);
       controller.setMainAppConnect(mainAppConnect);
       
       //Supprime les donn�es lors de la connexion
       controller.setUser(null);
       
    } catch (IOException e) {
        e.printStackTrace();
    }
		
	}
	
    public void showAdminAccueil(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/adminAccueil.fxml"));
            AnchorPane listeUser = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(listeUser);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            AdminAccueilController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
public void showListeUser(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/listeUser.fxml"));
            AnchorPane listeUser = (AnchorPane) loader.load();
            System.out.println(mainAppConnect);
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(listeUser);
            
         //  Donne au controller l'acc�s � mainAppConnect
           ListeUserController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void showListeEtablissement(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/listeEtablissement.fxml"));
            AnchorPane listeEtablissement = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(listeEtablissement);
         
         //  Donne au controller l'acc�s � mainAppAdmin
            ListeEtablissementController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showListeMatiere(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/listeMatiere.fxml"));
            AnchorPane listeMatiere = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            mainAppConnect.getConnect().setCenter(listeMatiere);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            ListeMatiereController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public void setMainAppConnect(MainAppConnect mainAppAdmin) {
        this.mainAppConnect = mainAppAdmin;
        
    }

	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user =user ;
	}
}
