package controller;

import java.sql.SQLException;

import dao.UserDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import metier.User;

/**
 * Author Marc Missler
 */
public class ListeUserProfController {


    @FXML
    private TableView<User> userTable;
    
    @FXML
    private TableColumn<User, String> roleTable;
    
    @FXML
    private TableColumn<User, String> prenomTable;

    @FXML
    private TableColumn<User, String> nomTable;

    @FXML
    private Label roleLabel;

    @FXML
    private Label prenomLabel;

    @FXML
    private Label nomLabel;

    @FXML
    private Label adressMailLabel;

    @FXML
    private Label loginLabel;

    @FXML
    private Label etablissementLabel;


    

    
    // Reference to the main application.

	private MainAppConnect mainAppConnect;
    



	public ListeUserProfController() {
	}
	
    /**
     * Initialise la class controlleur. Cette methode est automatiquement appell�e apr�s 
     * que le fichier FXML a �t� charg�.
     */
    @FXML
    private void initialize() {
        // Initialise la table User
    	updateTable() ;
    	roleTable.setCellValueFactory(cellData -> cellData.getValue().getRoleP());
         nomTable.setCellValueFactory(cellData -> cellData.getValue().getNomP());
         prenomTable.setCellValueFactory(cellData -> cellData.getValue().getPrenomP());
         
         
         // renitialise les details de personne
         showUserDetails(null);
         
         
         //cherche les changements de s�lection et affiche les d�tails de la personne quand ils sont modifi�s.
      userTable.getSelectionModel().selectedItemProperty().addListener(
                 (observable, oldValue, newValue) -> showUserDetails(newValue));
    }
    /**
     * Fills all text fields to show details about the person.
     * If the specified person is null, all text fields are cleared.
     *
     * @param person the person or null
     */
    private void showUserDetails(User user) {
        if (user != null) {
            // Remplir les label avec les informations de l'objet personne
            prenomLabel.setText(user.getPrenom());
            nomLabel.setText(user.getNom());
          //  roleLabel.setText(Integer.toString(user.getRole()));
            nomLabel.setText(user.getNom());
            roleLabel.setText(user.getRole());
            adressMailLabel.setText(user.getAdresse());
            etablissementLabel.setText(user.getEtablissement().getLibelle());
            loginLabel.setText(user.getLogin());
        
        } else {
            // Si User = null alors champs vide
            prenomLabel.setText("");
            nomLabel.setText("");
            roleLabel.setText("");
           loginLabel.setText("");
           adressMailLabel.setText("");
           etablissementLabel.setText("");
        }
        
    }

    /**
     * The data as an observable list of User.
     */
    private ObservableList<User> userData = FXCollections.observableArrayList();



    /**
     * Returns the data as an observable list of Persons. 
     * @return
     */
    public ObservableList<User> getUserData() {
        return userData;
    }
    
    public void updateTable() {
    	
    	try {
    		UserDAO userdao = new UserDAO() ;
    		//EtablissementDAO etablissementdao = new EtablissementDAO();
    		
    		ObservableList<User> liste = userdao.findAll();
    		//System.out.println(liste.get(0));
    		 userTable.setItems(liste);
    		
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (ClassNotFoundException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	
    }

    
        /**
         * Is called by the main application to give a reference back to itself.
         * 
         * @param mainAppProf
         */


        // Add observable list data to the table
      

    	public void setMainAppConnect(MainAppConnect mainAppAdmin) {
            this.mainAppConnect = mainAppAdmin;
            
        }
    
}
