package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import dao.ClasseDAO;
import dao.Connect;
import dao.EnseigneDAO;
import dao.EtablissementDAO;
import dao.GestionInterface;
import dao.MatiereDAO;
import dao.UserDAO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import metier.Classe;
import metier.Enseigne;
import metier.Etablissement;
import metier.Matiere;
import metier.User;
import java.security.*;
import javax.crypto.*;


/**
 * Author Marc Missler
 */
public class UserEditDialogController implements GestionInterface<User>{

    @FXML
    private TextField etablissementField;
    @FXML
    private TextField roleField;
    @FXML
    private TextField prenomField;
    @FXML
    private TextField loginField;
    @FXML
    private TextField nomField;
    @FXML
    private TextField adresseMailField;
    @FXML
    private PasswordField mdpPasswordField;
    @FXML
    private ComboBox<Etablissement> col_etablissement;
    
    private ComboBox<Matiere> col_matiere;

    private ComboBox<Classe> col_classe;
    @FXML
    private TextField ClasseField;
    @FXML
    private ComboBox<String> col_role;
    @FXML
    AnchorPane  edit ;
   
ListView<Classe> listeclasse ;
    ObservableList<Etablissement> empData  ;
    ObservableList<Classe> empData2  ;
    ObservableList<Matiere> empData3 ;
    ObservableList<Classe> classeSelect  ;

    private Stage dialogStage;
    private User user;
    private boolean okClicked = false;
	private Etablissement etablissementVal;
	private Classe classeVal;
	private String roleVal;
        @FXML
       GridPane grid ;
        private Matiere matiereVal;
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    classeSelect= FXCollections.observableArrayList() ;
    	ObservableList<String> roleData = FXCollections.observableArrayList() ;
      //roleData.add("[\"ROLE_ADMIN\"]");
    	roleData.add("[\"ROLE_PROFESSEUR\"]");
    	roleData.add("[\"ROLE_ELEVE\"]");
    	updateEtablissement();
    	col_role.setItems(roleData);
    	
    	col_role.valueProperty().addListener(new ChangeListener<String>() {

			

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue.contentEquals("[\"ROLE_PROFESSEUR\"]")) {
					roleVal = newValue;
					listeclasse = new ListView<>();
					//col_classe.resizeRelocate(50, 12,50, 50);
					col_matiere = new ComboBox<>();
					
					//   <ComboBox prefWidth="150.0" GridPane.columnIndex="1" GridPane.rowIndex="2" />
				    
					edit.getChildren().add(listeclasse);
					  grid.add(listeclasse, 1, 2);
					edit.getChildren().add(col_matiere);
					 grid.add(col_matiere, 1, 3);
					 
					
					
					    updateListeClasse();
						updateMatiere();
					
						listeclasse.getSelectionModel (). setSelectionMode (SelectionMode.MULTIPLE);
						listeclasse.setOnMouseClicked(new EventHandler<Event>() {

							@Override
							public void handle(Event event) {
								// TODO Auto-generated method stub
								 classeSelect =  listeclasse.getSelectionModel().getSelectedItems();

			                       
							}

		                    

		                });
						col_matiere.getSelectionModel().selectedItemProperty().addListener((o, ol, nw) -> 
		        		{matiereVal = col_matiere.getValue();
		    });
		     
				}else if(newValue.contentEquals("[\"ROLE_ELEVE\"]")) {
					
					roleVal = newValue;
					col_classe = new ComboBox<>();
					//col_classe.resizeRelocate(50, 12,50, 50);
					
					//   <ComboBox prefWidth="150.0" GridPane.columnIndex="1" GridPane.rowIndex="2" />
				    
					edit.getChildren().add(col_classe);
					  grid.add(col_classe, 1, 2);
					  update();
					  StringConverter<Classe> conv1 = new StringConverter<Classe>() {
			        		@Override
			        		public Classe fromString(String string) {
			        		return null;
			        		}
			        		@Override
			        		public String toString(Classe classe) {
			        		return classe.getLibelle();
			        		}
			        		};
			        		col_classe.setConverter(conv1);
			        		col_classe.getSelectionModel().selectedItemProperty().addListener((o, ol, nw) -> 
			        		{classeVal = col_classe.getValue();
			    });
					
				}
			
			}
    		
    	});
    	StringConverter<Etablissement> conv = new StringConverter<Etablissement>() {
    		@Override
    		public Etablissement fromString(String string) {
    		return null;
    		}
    		@Override
    		public String toString(Etablissement etab) {
    		return etab.getLibelle();
    		}
    		};
    		col_etablissement.setConverter(conv);
    		col_etablissement.getSelectionModel().selectedItemProperty().addListener((o, ol, nw) -> 
    		{etablissementVal =	col_etablissement.getValue();
});
    	
    		
    	
        		
        		
}
    
    public void updateEtablissement() {
    	  try {
  	    	EtablissementDAO EtablissementDAO = new EtablissementDAO();
  	    	Etablissement etablissement = new Etablissement();
  	    	
  	    	empData = EtablissementDAO.findLibelleEtablissement();
  	    	}catch (ClassNotFoundException | SQLException e) {

  	    	e.printStackTrace();
  	    	}

  	    	col_etablissement.setItems(empData);
  	    	
  	    	
    	
    }
    	
  public  void update() {
	  
	  try {
	    
	    	ClasseDAO ClasseDAO = new ClasseDAO();
	    	empData2 = ClasseDAO.findLibelleClasse();
	    	}catch (ClassNotFoundException | SQLException e) {

	    	e.printStackTrace();
	    	}
	    	col_classe.setItems(empData2);
	    	
  }
  public  void updateListeClasse() {
	  
	  try {
	    	
	    	ClasseDAO ClasseDAO = new ClasseDAO();
	    	
	    	empData2 = ClasseDAO.findLibelleClasse();
	    	}catch (ClassNotFoundException | SQLException e) {

	    	e.printStackTrace();
	    	}

	    
	    	
	    	listeclasse.setItems(empData2);
  }
  
public  void updateMatiere() {
	  
	 
	try {
	    	
		
    	MatiereDAO MatiereDAO = new MatiereDAO();
    	
    	empData3 = MatiereDAO.findLibelleMatiere();
    	}catch (ClassNotFoundException | SQLException e) {

    	e.printStackTrace();
    	}

    	
    	col_matiere.setItems(empData3);
  }
  
  

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     *
     * @param person
     */
    public void setUser(User user) {
        this.user = user;

        prenomField.setText(user.getPrenom());
        nomField.setText(user.getNom());
        loginField.setText(user.getLogin());
        adresseMailField.setText(user.getAdresse());
        mdpPasswordField.setText(user.getPassword());
       // etablissementField.setText(user.getEtablissement().getLibelle());
        col_etablissement.setValue(user.getEtablissement());
        //col_classe.setValue(user.getClasse());
        col_role.setValue(user.getRole());
        

    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    

    @FXML
    private  void handleOk() {
    	
    	if (isInputValid()) {
    		System.out.println(user+"okk");
        	if (user.getIdUser()== 0) {
        		User user = new User();
    			UserDAO userdao = new UserDAO();
    			 
	            user.setPrenom(prenomField.getText());
	            user.setNom(nomField.getText());
	            user.setLogin(loginField.getText());
	         //   user.setRole(Integer.parseInt(roleField.getText()));
	            user.setRole(roleVal);
	            user.setAdresse(adresseMailField.getText());
	            user.setPassword(mdpPasswordField.getText());
	            user.setEtablissement(etablissementVal);
	            if (roleVal.contentEquals("[\"ROLE_ELEVE\"]"))
	            {
	                 user.setClasse(classeVal);
	                 
	                 try {
	                	
	      	           int lastid =userdao.insert(user);
	      	           
	      	            
	      	            System.out.println(user.getLogin()+"2");
	      	            }catch (SQLException e) {
	      	            	// TODO Auto-generated catch block
	      					e.printStackTrace();
	      				} catch (ClassNotFoundException e) {
	      					// TODO Auto-generated catch block
	      					e.printStackTrace();
	      				}
	            }else if (roleVal.contentEquals("[\"ROLE_PROFESSEUR\"]")) {
	            	System.out.println("Etape 1");
	            	user.setMatiere(matiereVal);
	            	
	                try {
		            	user.setClasse(null);
		            	System.out.println(classeVal);
	     	           int lastid =userdao.insert(user);
	     	           System.out.println("last"+lastid);
	     	           
	     	           Enseigne enseigne = new Enseigne() ;
	     	           enseigne.setIdUser(lastid);
	     	           enseigne.setListeclasse(classeSelect);
	     	           EnseigneDAO enseigneDAO = new EnseigneDAO() ;
	     	           enseigneDAO.insert(enseigne);
	     	            
	     	            System.out.println(user.getLogin()+"2");
	     	            }catch (SQLException e) {
	     	            	// TODO Auto-generated catch block
	     					e.printStackTrace();
	     				} catch (ClassNotFoundException e) {
	     					// TODO Auto-generated catch block
	     					e.printStackTrace();
	     				}
	            	
	            }
	            System.out.println(user.getLogin()+"ok");
	        
	            }else {
	            	 user.setPrenom(prenomField.getText());
	 	            user.setNom(nomField.getText());
	 	            user.setLogin(loginField.getText());
	 	         //   user.setRole(Integer.parseInt(roleField.getText()));
	 	            user.setRole(roleVal);
	 	            user.setAdresse(adresseMailField.getText());
	 	            user.setPassword(mdpPasswordField.getText());
	 	            user.setEtablissement(etablissementVal);
	 	           user.setClasse(classeVal);
	 	           //System.out.println(user.getLogin());
	 	           UserDAO userdao = new UserDAO();
	            try {
					userdao.ModifEuser(user);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
            // update
            
        	}
            okClicked = true;
            dialogStage.close();
        
    }


    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (mdpPasswordField.getText() == null || mdpPasswordField.getText().length() == 0) {
            errorMessage += "Mot de passe non valide ! \n";
        }
        if (prenomField.getText() == null || prenomField.getText().length() == 0) {
            errorMessage += "Prenom non valide ! \n";
        }
        if (adresseMailField.getText() == null || adresseMailField.getText().length() == 0) {
            errorMessage += "Email non valide ! ! \n";
        }
        if (nomField.getText() == null || nomField.getText().length() == 0) {
            errorMessage += "Nom non valide !\n";
        }
        if (loginField.getText() == null || loginField.getText().length() == 0) {
            errorMessage += "Login non valide\n";
        }

        if (col_role.getValue() == null || col_role.getValue().length() == 0) {
            errorMessage += "Role non valide !\n";
        } 
        if (col_etablissement.getValue() == null ) {
            errorMessage += "Etablissement non valide\n";
        }
       
        else {
            
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Champs invalide");
            alert.setHeaderText("Veuillez corriger les champs");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

	@Override
	public int insert(User t) throws SQLException, ClassNotFoundException {

		return 0;

}
	

	@Override
	public int delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ObservableList<User> findAll() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<User> findBy(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<User> findByEleve() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}