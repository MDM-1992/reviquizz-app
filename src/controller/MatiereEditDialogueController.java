package controller;
import java.sql.SQLException;

import dao.MatiereDAO;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import metier.Matiere;

/**
 * @author Thomas Haloche
 */
public class MatiereEditDialogueController {


	private Stage dialogStage;
	private Matiere matiere ;
	private boolean okClicked;

    @FXML
    private Label libelle;

    @FXML
    private Button validerButton;

    @FXML
    private Button annulerButton;

    @FXML
    private TextField textfieldLibelle;

    @FXML
    private void initialize(){
    }
   
    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     *
     * @param person
     */
    public void setMatiere(Matiere matiere) {
    	
        this.matiere = matiere;
        	if ( matiere !=null) {
        textfieldLibelle.setText(matiere.getlibelle());
}
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
        	
        	if (matiere == null) {
        		matiere = new Matiere();
        		matiere.setlibelle(textfieldLibelle.getText());
            MatiereDAO matdao = new MatiereDAO();
            try {
				matdao.insert(matiere);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
        	}else {
				
				matiere.setlibelle(textfieldLibelle.getText());
				//System out print pour verifier si il envoie bien les donn�es
	           // System.out.println(matiere.getIdMatiere()+ " "+matiere.getlibelle());
	            MatiereDAO matdao = new MatiereDAO();
	            try {
					matdao.ModifMatiere(matiere);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
            // update
            
        	}
            okClicked = true;
            dialogStage.close();
        
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (textfieldLibelle.getText() == null || textfieldLibelle.getText().length() == 0) {
            errorMessage += "Matiere non valide !";
        }
        

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Donn�es invalides");
            alert.setHeaderText("Veuillez entrer les bonnes donn�es.");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}