package controller;

import java.io.IOException;

import application.MainAppAdmin;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metier.Matiere;
import metier.User;

/**
 * Author Marc Missler
 */
public class MainAppConnect extends Application  {
	private Stage primaryStage;
    private static BorderPane connect;
    private User user;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("reviquizz-app");
        try {
            // Charge l'adminMenu du fichier fxml.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getResource("/vue/connexion.fxml"));
            connect = (BorderPane) loader.load();
            ConnexionController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(this);
            //  Affiche la scene contenant l'adminMenu
            Scene scene = new Scene(connect);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public boolean showMatiereEditDialog(Matiere matiere) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getClassLoader().getResource("vue/MatiereEditDialogue.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Matiere");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            MatiereEditDialogueController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMatiere(matiere);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
 
    public static BorderPane getConnect() {
		return connect;
	}

	public static void setConnect(BorderPane connect) {
		connect = connect;
	}

	public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }


	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user = user ;
	}
   
    }

