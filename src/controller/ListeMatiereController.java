package controller;

import java.io.IOException;
import java.sql.SQLException;
import application.MainAppAdmin;
import dao.MatiereDAO;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import metier.Matiere;
import metier.User;




/**
 * @author Thomas Haloche
  * @notes Ce controller d�stin� aux administrateurs est utilis� pour la
  * gestion des mati�res
  * @functionality Affiche le detail des mati�res / 
  * Suppression, ajout et modification des mati�res .
  * 
 */
public class ListeMatiereController extends Application {


		private MainAppConnect mainAppConnect;
		private User user;

			@FXML
		    private TableView<Matiere> MatiereTable;

		    @FXML
		    private TableColumn<Matiere, String> MatiereCol;

	    @FXML
	    private Label libelleMatiere;

	    @FXML
	    private Button addButton;

	    @FXML
	    private Button deleteButton;

	    @FXML
	    private Button editButton;

	    @FXML
		private Stage primaryStage;

	    @FXML
	    private Label libelle;

	    @FXML
	    private Button handleOk;

	    @FXML
	    private Button handleCancel;

	    @FXML
	    private TextField textfieldLibelle;

	    private void showMatiereDetails(Matiere matiere) {
	        if (matiere != null) {
	            // Fill the labels with info from the person object.
	            libelle.setText(matiere.getlibelle());


	            // TODO: We need a way to convert the birthday into a String!
	            // birthdayLabel.setText(...);
	        } else {
	            // Person is null, remove all the text.
	            libelle.setText("");
	        }
	    }

	    @FXML
	    private void initialize() {
	        // Initialize la table etablissement avec les colonnes.
	    	
	    	
	    	updateTable();
	    	
	    	MatiereCol.setCellValueFactory(cellData -> cellData.getValue().getlibelleP());
	        
	    }

	    private void updateTable() {
			// TODO Auto-generated method stub
	    	MatiereTable.setItems(getMatiereData());
		}
	    
	    /**
	     * Les donn�es sont une observable list d'Etablissement.
	     */
	    private ObservableList<Matiere> MatiereData = FXCollections.observableArrayList();
	    
	    /**
	     * Renvoie les donn�es sous forme d' observable liste Etablissement. 
	     * @return
	     */
	    public ObservableList<Matiere> getMatiereData() {
	    	MatiereDAO matdao = new MatiereDAO();
	    	try {
				MatiereData = matdao.findAll() ;
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return MatiereData;
	    }
	    
	    /**
	     * Est appel� par le main application pour renvoyer une r�f�rence.
	     *
	     * @param mainApp
	     */


	        // Add observable list data to the table
	      
	    
	    /**
	     * Appell� quand l'admin clique sur le boutton delete.
	     */
	    @FXML
	    private void handleDeleteMatiere() {
	    	Matiere matiere =  MatiereTable.getSelectionModel().getSelectedItem();
	    	MatiereDAO matiered = new MatiereDAO();
	    	int nbligne =0;
	    	if (matiere != null) {
	    		  
	    	try {
				 nbligne = matiered.delete(matiere.getIdMatiere());
				updateTable() ;
				 
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
	    	}else {
	            // Alerte si aucun etablissement n'est s�lectionn�. 
	            Alert alert = new Alert(AlertType.WARNING);
	            alert.initOwner(mainAppConnect.getPrimaryStage());
	            alert.setTitle("Pas de selection");
	            alert.setHeaderText("Pas d'etablissement s�lectionn�");
	            alert.setContentText("Veuillez selectionner une matiere.");
	            alert.showAndWait();
	        }
	       if ( nbligne == 1)
	       {
	    	   
	    	   Alert alert = new Alert(AlertType.INFORMATION);
	    
	           alert.initOwner(mainAppConnect.getPrimaryStage());
	           alert.setTitle("Suppression ");
	           alert.setHeaderText("L'etablissement que vous avez s�l�ctionn� � �t� supprim�.");
	           alert.setContentText("Vous vennez de supprimer une matiere.");
	           alert.showAndWait();
	        } 
	    }
	    

		/**
	     * Appel� quand l'administrateur clique sur le boutton nouveau.
	     * Ouverture de dialogue pour appliquer un nouvelle etablissement.
	     */
	    @FXML
	    private void handleNewMatiere() {
	        Matiere tempMatiere = null;
	        boolean okClicked = mainAppConnect.showMatiereEditDialog(tempMatiere);
	        if (okClicked) {
	        	
	        	updateTable() ;
	        }
	    }

	    /**
	     * Appel� quand l'administrateur clique sur le boutton modifier. 
	     * Ouverture de dialogue pour modifier les details d'un �tablissement.
	     */
	    @FXML
	    private void handleEditMatiere() {
	        Matiere selectedMatiere = MatiereTable.getSelectionModel().getSelectedItem();
	        System.out.println(selectedMatiere);
	        if (selectedMatiere != null) {
	            boolean okClicked = mainAppConnect.showMatiereEditDialog(selectedMatiere);
	            if (okClicked) {
	            
	                showMatiereDetails(selectedMatiere);
	                Alert alert = new Alert(AlertType.INFORMATION);
	                alert.initOwner(mainAppConnect.getPrimaryStage());
	                alert.setTitle("Modification");
	                alert.setHeaderText("Modification d'une matiere");
	                alert.setContentText("Vous venez d'effectuer une modification");
	                
	                alert.showAndWait();
	            }

	        } else {
	            // Nothing selected.
	            Alert alert = new Alert(AlertType.WARNING);
	            alert.initOwner(mainAppConnect.getPrimaryStage());
	            alert.setTitle("Aucune s�lection");
	            alert.setHeaderText("Pas de matiere s�lectionn�");
	            alert.setContentText("Veuillez s�lectionner une matiere.");

	            alert.showAndWait();
	        }
	    }

	    public void setMainAppConnect(MainAppConnect mainAppAdmin) {
	        this.mainAppConnect = mainAppAdmin;
	        
	    }

		@Override
		public void start(Stage arg0) throws Exception {
			// TODO Auto-generated method stub
			
		}
	    

		public void setUser(User user) {
			// TODO Auto-generated method stub
			this.user =user ;
		}

	}
	    
	    
	    
	    
	    
	    
	    
