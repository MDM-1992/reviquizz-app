package controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import metier.User;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class ProfMenuController {

    @FXML
    private Button btnUser;
    
    @FXML
    private Button btnDeconnexion;
    

    @FXML
    private Button btnQuizz;

    @FXML
    private Button btnAccueil;
    
	private MainAppConnect mainAppConnect;
	private User user  ;
	
	
	/**
	 *Renvoi l'utilisateur � la page de connexion lors de la d�connexion.
	 */
	public void showConnexion(ActionEvent evt) {
	       try {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainAppConnect.class.getResource("/vue/connexion.fxml")); 
        BorderPane connect  = (BorderPane) loader.load();
        
        //Supprime les scenes de AdminAccueil et AdminMenu lors de la deconnexion
        MainAppConnect.getConnect().setLeft(null);
        MainAppConnect.getConnect().setCenter(null);
      
        MainAppConnect.getConnect().setCenter(connect);
        ConnexionController controller = loader.getController();
        System.out.println(controller);
       controller.setMainAppConnect(mainAppConnect);
       
       //Supprime les donn�es lors de la connexion
       controller.setUser(null);
       
    } catch (IOException e) {
        e.printStackTrace();
    }
		
	}
	
	public void showListeClasse(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/listeClasse.fxml"));
            AnchorPane listeUserProf = (AnchorPane) loader.load();
            System.out.println(mainAppConnect);
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(listeUserProf);
            
         //  Donne au controller l'acc�s � mainAppConnect
           ListeClasseController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	public void showListeQuizz(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/listeQuizz.fxml"));
            AnchorPane listeQuizz = (AnchorPane) loader.load();
            System.out.println(mainAppConnect);
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(listeQuizz);
            
         //  Donne au controller l'acc�s � mainAppConnect
           ListeQuizzController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showProfAccueil(ActionEvent evt) {
        try {
            // Charge la listeUser.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppConnect.class.getResource("/vue/ProfAccueil.fxml"));
            AnchorPane listeUser = (AnchorPane) loader.load();
            
            // Place la listeUser au centre de l'admin menu
            MainAppConnect.getConnect().setCenter(listeUser);
            
         //  Donne au controller l'acc�s � mainAppAdmin
            ProfAccueilController controller = loader.getController();
            System.out.println(controller);
           controller.setMainAppConnect(mainAppConnect);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	public void setMainAppConnect(MainAppConnect mainAppConnect) {
        this.mainAppConnect = mainAppConnect;
        
    }

	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user =user ;
	}

}
