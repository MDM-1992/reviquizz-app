package controller;

import java.sql.SQLException;

import dao.ClasseDAO;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import metier.Classe;
import metier.User;

public class ClasseEditDialogController {
	 /*
	 * Author Anaya Kassamaly
	 */

	    @FXML
	    private TextField classeField;

	    @FXML
	    private Label classeLabel;
	
	   


	    private User user;
	    private Stage dialogStage;
	    private Classe classe ;
	    private boolean okClicked = false;

	    /**
	     * Initializes the controller class. This method is automatically called
	     * after the fxml file has been loaded.
	     */
	    @FXML
	    private void initialize() {
	    }

	    /**
	     * Sets the stage of this dialog.
	     *
	     * @param dialogStage
	     */
	    public void setDialogStage(Stage dialogStage) {
	        this.dialogStage = dialogStage;
	    }

	    /**
	     * Sets the person to be edited in the dialog.
	     *
	     * @param person
	     */
	    public void setClasse(Classe classe) {
	    	
	        this.classe = classe;
	        	if ( classe !=null) {
	        classeField.setText(classe.getLibelle());

	}
	    }

	    /**
	     * Returns true if the user clicked OK, false otherwise.
	     *
	     * @return
	     */
	    public boolean isOkClicked() {
	        return okClicked;
	    }

	    /**
	     * Called when the user clicks ok.
	     */
	    @FXML
	    private void handleOk() {
	        if (isInputValid()) {
	        	
	        	if (classe == null) {
	        		classe = new Classe();
	        		classe.setLibelle(classeField.getText());

	            ClasseDAO classedao = new ClasseDAO();
	            try {
	            	classedao.insert(classe);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
	        	}else {
					
	        		classe.setLibelle(classeField.getText());
		            ClasseDAO classedao = new ClasseDAO();
		            try {
		            	classedao.ModifClasse(classe);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
	            // update
	            
	        	}
	            okClicked = true;
	            dialogStage.close();
	        
	    }

	    /**
	     * Called when the user clicks cancel.
	     */
	    @FXML
	    private void handleCancel() {
	        dialogStage.close();
	    }

	    /**
	     * Validates the user input in the text fields.
	     *
	     * @return true if the input is valid
	     */
	    private boolean isInputValid() {
	        String errorMessage = "";

	        if (classeField.getText() == null || classeField.getText().length() == 0) {
	            errorMessage += "Classe non valide !";
	        }
	        
	        if (errorMessage.length() == 0) {
	            return true;
	        } else {
	            // Show the error message.
	            Alert alert = new Alert(AlertType.ERROR);
	            alert.initOwner(dialogStage);
	            alert.setTitle("Donn�es invalides");
	            alert.setHeaderText("Veuillez entrer les bonnes donn�es.");
	            alert.setContentText(errorMessage);

	            alert.showAndWait();

	            return false;
	        }
	    }
		public void setUser(User user) {
			// TODO Auto-generated method stub
			this.user =user ;
		}
}
