package application;


import java.io.IOException;

import controller.ListeMatiereController;
import controller.MatiereEditDialogueController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
//import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
//import metier.User;
import metier.Matiere;

    //-----------------------------------Matiere---------------------------

    public class MainAppAdmin extends Application {

    	@FXML
        private Stage primaryStage;
    	@FXML
        private BorderPane adminMenu;
    	@FXML
    	private FXMLLoader loader;

        @Override
        public void start(Stage primaryStage) {
            this.primaryStage = primaryStage;
            this.primaryStage.setTitle("reviquizz-app");

            initadminMenu();
            //showListeUser();
        }
        
        /**
         * Initializes the root layout.
         */
        public void initadminMenu() {
            try {
                // Load root layout from fxml file.
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainAppAdmin.class.getResource("/vue/adminMenu.fxml"));
                adminMenu = (BorderPane) loader.load();
                
                // Show the scene containing the root layout.
                Scene scene = new Scene(adminMenu);
                primaryStage.setScene(scene);
                primaryStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

  
        
        /*private static void setCenter(Object etablissement) {
    		// TODO Auto-generated method stub
    		
    	}*/

    	/**
         * Returns the main stage.
         * @return
         */
        public Stage getPrimaryStage() {
            return primaryStage;
        }

        public static void main1(String[] args) {
            launch(args);
        }

    /**
     * The data as an observable list of Persons.
     */
    private ObservableList<Matiere> MatiereData = FXCollections.observableArrayList();   
      

    /**
     * Returns the data as an observable list of Persons. 
     * @return
     */
    public ObservableList<Matiere> getMatiereData() {
        return MatiereData;
    }

    /**
     * Opens a dialog to edit details for the specified person. If the user
     * clicks OK, the changes are saved into the provided person object and true
     * is returned.
     *
     * @param person the person object to be edited
     * @return true if the user clicked OK, false otherwise.
     */
    public boolean showMatiereEditDialog(Matiere matiere) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainAppAdmin.class.getClassLoader().getResource("vue/MatiereEditDialogue.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Matiere");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            MatiereEditDialogueController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMatiere(matiere);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    }

    //-----------------------------------------Fin Etablissement ---------------------------------------
      /*
        /**
         * Initialize  l'adminMenu.
         */
       /* 
        public void initAdminMenu() {
            try {
                // Charge l'adminMenu du fichier fxml.
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainAppAdmin.class.getResource("/vue/adminMenu.fxml"));
                adminMenu = (BorderPane) loader.load();
                
                //  Affiche la scene contenant l'adminMenu
                Scene scene = new Scene(adminMenu);
                primaryStage.setScene(scene);
                primaryStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
       
    // ======================================================ListUSER============================================================
        /**
         *  Affiche la listUser dans l'adminMenu.
         */
        
        
       /* public void showListeUser() {
            try {
                // Charge la listeUser.
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainAppAdmin.class.getResource("/vue/listeUser.fxml"));
                AnchorPane listeUser = (AnchorPane) loader.load();
                
                // Place la listeUser au centre de l'admin menu
                adminMenu.setCenter(listeUser);
                
             //  Donne au controller l'acc�s � mainAppAdmin
                ListeUserController controller = loader.getController();
                System.out.println(controller);
                controller.setMainAppAdmin(this);
                
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        
        /**
         * The data as an observable list of Persons.
         */
        /*private ObservableList<User> userData = FXCollections.observableArrayList();

        
        /**
         * Retourne les donn�es en tant qu'observable list de user
         * @return
         */
        /*public ObservableList<User> getUserData() {
            return userData;
        }

     

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>UserEditDialog>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        /**
         * Opens a dialog to edit details for the specified person. If the user
         * clicks OK, the changes are saved into the provided person object and true
         * is returned.
         *
         * @param person the person object to be edited
         * @return true if the user clicked OK, false otherwise.
         */
       /*public boolean showUserEditDialog (User user ) {
            try {
                // Load the fxml file and create a new stage for the popup dialog.
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainAppAdmin.class.getResource("/vue/UserEditDialog.fxml"));
                AnchorPane page = (AnchorPane) loader.load();

                // Create the dialog Stage.
                Stage dialogStage = new Stage();
                dialogStage.setTitle("Edit User");
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.initOwner(primaryStage);
                Scene scene = new Scene(page);
                dialogStage.setScene(scene);

                // Set the person into the controller.
                UserEditDialogController controller = loader.getController();
                controller.setDialogStage(dialogStage);
               Object Etablissement = null;
    			// controller.setUser(user);
                controller.setEtablissement(user);

                // Show the dialog and wait until the user closes it
                dialogStage.showAndWait();

                return controller.isOkClicked();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
       }
        
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>FinUserEditDialog>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        //===================================================finListUSER===================================================

        /**
         *retourne le main stage.
         * @return
         */
       /* public Stage getPrimaryStage() {
            return primaryStage;
        }

        public static void main(String[] args) {
            launch(args);
        }
    }
    */