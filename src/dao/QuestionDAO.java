package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import metier.Classe;
import metier.Matiere;
import metier.Question;
import metier.Questionnaire;
import metier.User;

public class QuestionDAO implements GestionInterface<Question>{
	@Override
	public int insert(Question question) throws SQLException {

		Connection cnx = Connect.getInstance().getConnection() ;
		
		String req = "INSERT INTO question (idQuestionnaire, libelleQuestion,numQuestion) VALUES (?,?,?)";
		PreparedStatement  stm =  cnx.prepareStatement(req,Statement.RETURN_GENERATED_KEYS);
		stm.setInt(1, question.getQuestionnaire().getIdQuestionnaire());
		stm.setString(2, question.getLibelle());
		stm.setInt(3, question.getNumQuestion());
		int nbligne = stm.executeUpdate();
		ResultSet test = stm.getGeneratedKeys();
		int key = 0;
		while(test.next()) {
			key = test.getInt(1);
		}		
		stm.close();
		test.close();
		cnx.close();
		
		return key;		
	}
	
	public ArrayList<Question> recupQuestions(int idQuestionnaire) throws SQLException {
		ArrayList<Question> questionList = new ArrayList<Question>();
		String req = "SELECT idQuestion, libelleQuestion, numQuestion FROM question WHERE question.idQuestionnaire = ?" ;
		Connection cnx = Connect.getInstance().getConnection() ;
		PreparedStatement stm = cnx.prepareStatement(req);
		stm.setInt(1, idQuestionnaire);
		
		int id;
	    String libelle;
	    int numQuestion;
	      
	    ResultSet jeu = stm.executeQuery();
	    while(jeu.next()){
	    	id = jeu.getInt("idQuestion");
	        libelle = jeu.getString("libelleQuestion");
	        numQuestion = jeu.getInt("numQuestion");
	        	 
	        Question question = new Question();
	        question.setLibelle(libelle);
	        question.setIdQuestion(id);
	        question.setNumQuestion(numQuestion);
	        questionList.add(question);

	    }
	    jeu.close();
      	stm.close();
        cnx.close();
		return questionList;
	}
	
	@Override
	public int delete(int id) throws SQLException {
		Connection cnx =  Connect.getInstance().getConnection() ;
		
		String req = "DELETE FROM `question` WHERE idQuestion = ?";

		PreparedStatement  stm =  cnx.prepareStatement(req);
			
		stm.setInt(1, id);
				
		int nbligne = stm.executeUpdate();
				
		return nbligne;

	}// je suis nul et méchante jeu ses pos parrlai frantskest


	@Override
	public ObservableList<Question> findAll() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;

	}

	@Override
	public ObservableList<Question> findBy(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<User> findByEleve() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
