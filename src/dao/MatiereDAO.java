package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import metier.Classe;
import metier.Matiere;
import metier.User;

/**
 * Author Thomas Haloche
 */
public class MatiereDAO implements GestionInterface<Matiere>{
	@Override
	public int insert(Matiere matiere) throws SQLException {

		Connection cnx = Connect.getInstance().getConnection() ;
		
		String req = "INSERT INTO `matiere`( `idMatiere`,`libelle`) VALUES (?,?) ";
		PreparedStatement  stm =  cnx.prepareStatement(req);

		stm.setInt(1, matiere.getIdMatiere());
		stm.setString(2, matiere.getlibelle());
	
		int nbligne = stm.executeUpdate();
			
		return nbligne;	
	}

	public static void insertMatiere(Matiere matiere) throws SQLException, ClassNotFoundException {
		// Je me connecte
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te inserer new pers
		String requeteSQL = "INSERT INTO `matiere`(`idMatiere`,`libelle`) "
				+ "VALUES (?,?)";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setInt(1, matiere.getIdMatiere());
		pst.setString(2, matiere.getlibelle());
		
		int nbligne = pst.executeUpdate();

	}
	

	public  void ModifMatiere(Matiere matiere) throws SQLException, ClassNotFoundException {
		// Je me connecte
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te inserer new pers
		String requeteSQL = "UPDATE matiere SET  libelle= ?  WHERE idMatiere = ?";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setInt(1, matiere.getIdMatiere());
		pst.setString(2, matiere.getlibelle());
		
	
		int nbligne = pst.executeUpdate();

	
		
	}

	@Override
	public ObservableList<Matiere> findAll() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		String req = "select * from Matiere";
		Connection co = Connect.getInstance().getConnection();
		int id;
		String Libelle;
		
		ObservableList<Matiere> listematiere = FXCollections.observableArrayList();
		PreparedStatement pst = co.prepareStatement(req);
		
		ResultSet table = pst.executeQuery();
		while(table.next()) {
			id = table.getInt("idMatiere");
			Libelle = table.getString("Libelle");
			
			
			Matiere matiere = new Matiere();
			matiere.setIdMatiere(id);
			matiere.setlibelle(Libelle);
			
			listematiere.add(matiere);
		}
		table.close();
		pst.close();
		co.close();
		return listematiere;
	}

	@Override
	public int delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te supprimer etablissement
		String requeteSQL = "DELETE FROM `matiere` WHERE idMatiere = ?";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setInt(1, id);
		
		
		
		int i = pst.executeUpdate();

		return i;
	}
	


	
		//requ�te pour r�cup�rer seulement le libelle etablissement pour la comboBox de UserEditDialog
		public ObservableList<Matiere> findLibelleMatiere() throws ClassNotFoundException, SQLException {
			ObservableList<Matiere> listeMatiere = FXCollections.observableArrayList();
			Connection cnx = Connect.getInstance().getConnection();
			String req = "select idMatiere , libelle from matiere";
			PreparedStatement pst = cnx.prepareStatement(req);
			ResultSet jeu = pst.executeQuery();
			while(jeu.next()){
			Matiere matiere = new Matiere();
			matiere.setlibelle(jeu.getString("libelle"));
			matiere.setIdMatiere(jeu.getInt("idMatiere"));
			listeMatiere.add(matiere);
			}
			
	        jeu.close();
	        pst.close();
	        cnx.close();
			return listeMatiere;
	}

		@Override
		public ObservableList<Matiere> findBy(int id) throws ClassNotFoundException, SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ObservableList<User> findByEleve() throws ClassNotFoundException, SQLException {
			// TODO Auto-generated method stub
			return null;
		}
}

