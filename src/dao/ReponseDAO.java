package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javafx.collections.ObservableList;
import metier.Reponse;
import metier.User;

public class ReponseDAO implements GestionInterface<Reponse> {
	@Override
	public int insert(Reponse reponse) throws SQLException {

		Connection cnx = Connect.getInstance().getConnection() ;
		
		String req = "INSERT INTO reponse (idQuestion, libelle, valeur) VALUES (?,?,?)";
		PreparedStatement  stm =  cnx.prepareStatement(req);
		
		stm.setInt(1, reponse.getQuestion().getIdQuestion());
		stm.setString(2, reponse.getLibelle());
		stm.setBoolean(3, reponse.getValeur());
		int nbligne = stm.executeUpdate();
			
		return nbligne;	
	}

	@Override
	public int delete(int id) throws SQLException {
		Connection cnx =  Connect.getInstance().getConnection() ;
		
		String req = "DELETE FROM `reponse` WHERE idReponse = ?";

		PreparedStatement  stm =  cnx.prepareStatement(req);
			
		stm.setInt(1, id);
				
		int nbligne = stm.executeUpdate();
				
		return nbligne;
	}


	@Override
	public ObservableList<Reponse> findAll() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<Reponse> findBy(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<User> findByEleve() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
