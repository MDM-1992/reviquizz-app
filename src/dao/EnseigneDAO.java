package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javafx.collections.ObservableList;
import metier.Classe;
import metier.Enseigne;
import metier.User;

public class EnseigneDAO implements GestionInterface<Enseigne>{

	public   int insert(Enseigne enseigne) throws ClassNotFoundException, SQLException {
		
/* @FXML
	private String password;*/
int nbligne =0;
		Connection cnx = Connect.getInstance().getConnection() ;
for ( Classe classe : enseigne.getListeclasse()) {
		String req = "INSERT INTO `enseigne`(`id`, `id_classe`, `id_user`) VALUES (?,?,?)";
		PreparedStatement  stm =  cnx.prepareStatement(req);

		stm.setInt(1, enseigne.getId() );
		stm.setInt(2, classe.getIdClasse() );
		stm.setInt(3, enseigne.getIdUser() );
	    nbligne = stm.executeUpdate();
		nbligne ++ ;
	}
		return nbligne ;



	}

	@Override
	public int delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ObservableList<Enseigne> findAll() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<Enseigne> findBy(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<User> findByEleve() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}