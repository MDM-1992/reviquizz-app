package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javafx.collections.ObservableList;
import metier.Niveau;
import metier.User;

public class NiveauDAO implements GestionInterface<Niveau>{
	@Override
	public int insert(Niveau niveau) throws SQLException {

		Connection cnx = Connect.getInstance().getConnection() ;
		
		String req = "INSERT INTO niveau (libelle) VALUES (?)";
		PreparedStatement  stm =  cnx.prepareStatement(req);
		
		stm.setString(1, niveau.getLibelle());
		int nbligne = stm.executeUpdate();
			
		return nbligne;	
	}

	@Override
	public int delete(int id) throws SQLException {
		Connection cnx =  Connect.getInstance().getConnection() ;
		
		String req = "DELETE FROM `niveau` WHERE idNiveau = ?";
		
		//gestion des suppressions de questions et r�ponses li�s au questionnaire

		PreparedStatement  stm =  cnx.prepareStatement(req);
			
		stm.setInt(1, id);
				
		int nbligne = stm.executeUpdate();
				
		return nbligne;
	}

	@Override
	public ObservableList<Niveau> findAll() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<Niveau> findBy(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<User> findByEleve() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
