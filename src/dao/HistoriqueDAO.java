package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javafx.collections.ObservableList;
import metier.HistoriqueQCM;
import metier.User;

/**
 * Author: Jeremy Roche
 */
public class HistoriqueDAO implements GestionInterface<HistoriqueQCM>{
	@Override
	public int insert(HistoriqueQCM historiqueqcm) throws SQLException {

		Connection cnx = Connect.getInstance().getConnection() ;
		
		String req = "INSERT INTO historiqueqcm(idEtudiant,date,resultat) VALUES (?,?,?)";
		PreparedStatement  stm =  cnx.prepareStatement(req);

		stm.setInt(1,historiqueqcm.getUser().getIdUser());
		stm.setDate(2,historiqueqcm.getDateQCM());
		stm.setInt(3,historiqueqcm.getResultat());
		int nbligne = stm.executeUpdate();
			
		return nbligne;	
	}

	@Override
	public int delete(int id) throws SQLException {
		Connection cnx =  Connect.getInstance().getConnection() ;
		
		String req = "DELETE FROM `historiqueqcm` WHERE idHisto = ?";

		PreparedStatement  stm =  cnx.prepareStatement(req);
			
		stm.setInt(1, id);
				
		int nbligne = stm.executeUpdate();
				
		return nbligne;	
	}


	@Override
	public ObservableList<HistoriqueQCM> findAll() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<HistoriqueQCM> findBy(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<User> findByEleve() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
