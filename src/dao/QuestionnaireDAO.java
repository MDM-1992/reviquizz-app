package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import metier.Classe;
import metier.Matiere;
import metier.Niveau;
import metier.Question;
import metier.Questionnaire;
import metier.User;

public class QuestionnaireDAO implements GestionInterface<Questionnaire> {
	@Override
	public int insert(Questionnaire questionnaire) throws SQLException {

		Connection cnx = Connect.getInstance().getConnection() ;
		
		String req = "INSERT INTO questionnaire (idMatiere,idClasse, libelle,nbQuestions) VALUES (?,?,?,?)";
		PreparedStatement  stm =  cnx.prepareStatement(req,Statement.RETURN_GENERATED_KEYS);
		
		stm.setInt(1, 1);
		//stm.setInt(1, questionnaire.getMatiere().getIdMatiere());
		stm.setInt(2, 1);
		stm.setString(3, questionnaire.getLibelle());
		stm.setInt(4, questionnaire.getNbQuestions());
		int nbligne = stm.executeUpdate();
		ResultSet test = stm.getGeneratedKeys();
		int key = 0;
		while(test.next()) {
			key = test.getInt(1);
		}		
		stm.close();
		cnx.close();
		test.close();
		
		return key;	
		
	} 

	@Override
	public int delete(int id) throws SQLException {
		Connection cnx =  Connect.getInstance().getConnection() ;
		
		String req = "DELETE FROM `questionnaire` WHERE idQuestionnaire = ?";
		
		//gestion des suppressions de questions et r�ponses li�s au questionnaire

		PreparedStatement  stm =  cnx.prepareStatement(req);
			
		stm.setInt(1, id);
				
		int nbligne = stm.executeUpdate();
				
		cnx.close();
		stm.close();
		return nbligne;
	}

	@Override
	public ObservableList<Questionnaire> findAll() throws ClassNotFoundException, SQLException {
		ObservableList<Questionnaire> QuestionnaireList = FXCollections.observableArrayList();
        // constitution d'une commande base sur une requte SQL 
        // en vue d'tre excute sur une connexion donne     
        String req = "SELECT  idQuestionnaire, idMatiere, idClasse, libelle, nbQuestions FROM questionnaire" ;
        Connection cnx = Connect.getInstance().getConnection() ;
      int idQuestionnaire ;
      int idMatiere;
      int idClasse;
      String libelle ;
      int nbQuestions;
        PreparedStatement pst = cnx.prepareStatement(req);

        ResultSet jeu = pst.executeQuery();
        while(jeu.next()){
        idQuestionnaire = jeu.getInt("idQuestionnaire");
       	 idMatiere = jeu.getInt("idMatiere");
         idClasse = jeu.getInt("idClasse");
       	 libelle = jeu.getString("libelle");
       	 nbQuestions = jeu.getInt("nbQuestions");
       
       	 Matiere matiere = new Matiere();
       	 matiere.setIdMatiere(idMatiere);
       	 matiere.setlibelle(libelle);
       	 
       	 Classe classe = new Classe();
       	classe.setIdClasse(idClasse);
       	classe.setLibelle(libelle);
       	 
       	 Questionnaire questionnaire = new Questionnaire();
       	 questionnaire.setIdQuestionnaire(idQuestionnaire);
       	 questionnaire.setMatiere(matiere);
       	 questionnaire.setClasse(classe);
       	 questionnaire.setLibelle(libelle);
       	 questionnaire.setNbQuestions(nbQuestions);
       	 QuestionnaireList.add(questionnaire);
        }
        jeu.close();
        pst.close();
        cnx.close();
        return QuestionnaireList;

	}
	
	public Questionnaire recupQuestionnaire(int idQuestionnaire) throws SQLException {
		Questionnaire questionnaireRecup = new Questionnaire();
		
		String req = "SELECT idQuestionnaire, idMatiere, idClasse, libelle, nbQuestions FROM questionnaire WHERE questionnaire.idQuestionnaire = ?" ;
		Connection cnx = Connect.getInstance().getConnection() ;
		PreparedStatement stm = cnx.prepareStatement(req);
		stm.setInt(1, idQuestionnaire);
		
		int id;
	    int idMatiere;
	    int idClasse;
	    String libelle;
	    int nbQuestions;
	      
	    ResultSet jeu = stm.executeQuery();
	    while(jeu.next()){
	    	id = jeu.getInt("idQuestionnaire");
	        idMatiere = jeu.getInt("idMatiere");
	        idClasse = jeu.getInt("idClasse");
	        libelle = jeu.getString("libelle");
	        nbQuestions = jeu.getInt("nbQuestions");
	        	 
	 	    Matiere matiere = new Matiere();
	       	matiere.setIdMatiere(idMatiere);
	       	matiere.setlibelle(libelle);
	       	
	       	Classe classe = new Classe();
	        classe.setIdClasse(idClasse);
	        classe.setLibelle(libelle);

	 		questionnaireRecup.setIdQuestionnaire(id);
	 		questionnaireRecup.setMatiere(matiere);
	 		questionnaireRecup.setClasse(classe);
	 		questionnaireRecup.setLibelle(libelle);
	 		questionnaireRecup.setNbQuestions(nbQuestions);
	    }
	    jeu.close();
      	stm.close();
        cnx.close();
		return questionnaireRecup;
	}

	@Override
	public ObservableList<Questionnaire> findBy(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<User> findByEleve() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
