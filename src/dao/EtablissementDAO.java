package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import controller.ListeEtablissementController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import metier.Etablissement;
import metier.User;

/**
 * Author Anaya Kassamaly
 */
public class EtablissementDAO implements GestionInterface<Etablissement>{
	
	@Override
	public int insert(Etablissement etablissement) throws SQLException {

		Connection cnx = Connect.getInstance().getConnection() ;
		


		String req = "INSERT INTO etablissement (id, libelle, codePostal, adresse, ville) VALUES (?,?,?,?,?)";

		PreparedStatement  stm =  cnx.prepareStatement(req);

		stm.setInt(1, etablissement.getIdEtablissement());
		stm.setString(2, etablissement.getLibelle());
		stm.setInt(3, etablissement.getCodePostal());
		stm.setString(4, etablissement.getAdresse());
		stm.setString(5, etablissement.getVille());

	
		int nbligne = stm.executeUpdate();
			
		return nbligne;	
	}

	public static void insertEtablissement(Etablissement etablissement) throws SQLException, ClassNotFoundException {
		// Je me connecte
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te inserer new pers
		String requeteSQL = "INSERT INTO `etablissement`(`libelle`, `codePostal`, `adresse`, `ville`) "
				+ "VALUES (?,?,?,?)";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setString(1, etablissement.getLibelle());
		pst.setInt(2, etablissement.getCodePostal());
		pst.setString(3, etablissement.getadresse());
		pst.setString(4, etablissement.getville());

		int nbligne = pst.executeUpdate();

	}
	

	public  void ModifEtablissement(Etablissement etablissement) throws SQLException, ClassNotFoundException {
		// Je me connecte
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te inserer new pers
		String requeteSQL = "UPDATE etablissement SET libelle= ?,codePostale= ?, adresse = ?,ville= ? WHERE id =?";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setString(1, etablissement.getLibelle());
		pst.setInt(2, etablissement.getCodePostal());
		pst.setString(3, etablissement.getadresse());
		pst.setString(4, etablissement.getville());
		pst.setInt(5, etablissement.getIdEtablissement());
		int nbligne = pst.executeUpdate();

	
		
	}


	//requ�te pour r�cup�rer seulement le libelle etablissement pour la comboBox de UserEditDialog
	public ObservableList<Etablissement> findLibelleEtablissement() throws ClassNotFoundException, SQLException {
		ObservableList<Etablissement> listeE = FXCollections.observableArrayList();
		Connection cnx = Connect.getInstance().getConnection();
		String req = "select id , libelle from etablissement";
		PreparedStatement pst = cnx.prepareStatement(req);
		ResultSet jeu = pst.executeQuery();
		while(jeu.next()){
		Etablissement etab = new Etablissement();
		etab.setLibelle(jeu.getString("libelle"));
		etab.setIdEtablissement(jeu.getInt("id"));
		listeE.add(etab);
		}
		
        jeu.close();
        pst.close();
        cnx.close();
		return listeE;
		}


	@Override
	public ObservableList<Etablissement> findAll() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		String req = "select * from Etablissement";
		Connection co = Connect.getInstance().getConnection();
		int id;
		String Libelle;
		int codePostal;
		String adresse;
		String ville;
		ObservableList<Etablissement> listeEtablissement = FXCollections.observableArrayList();
		PreparedStatement pst = co.prepareStatement(req);
		
		ResultSet table = pst.executeQuery();
		while(table.next()) {
			id = table.getInt("id");
			Libelle = table.getString("Libelle");
			codePostal = table.getInt("CodePostal");
			adresse = table.getString("adresse");
			ville = table.getString("ville");
			
			Etablissement etablissement = new Etablissement();
			etablissement.setIdEtablissement(id);
			etablissement.setLibelle(Libelle);
			etablissement.setcodePostal(codePostal);
			etablissement.setadresse(adresse);
			etablissement.setville(ville);
			listeEtablissement.add(etablissement);
		}
		table.close();
		pst.close();
		co.close();
		return listeEtablissement;
	}

	@Override
	public int delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te supprimer etablissement
		String requeteSQL = "DELETE FROM `etablissement` WHERE id = ?";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setInt(1, id);
		
		
		
		int i = pst.executeUpdate();

		return i;
	}


	@Override
	public ObservableList<Etablissement> findBy(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<User> findByEleve() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}





}
