package dao;

import java.sql.SQLException;
import java.sql.Statement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import metier.Classe;
import metier.Etablissement;
import metier.User;

/**
 * Author Marc Missler
 */
public class UserDAO implements GestionInterface<User>{

		public   int insert(User user) throws ClassNotFoundException, SQLException {
			 System.out.println(user.getAdresse());
	/* @FXML
		private String password;*/

			Connection cnx = Connect.getInstance().getConnection() ;
			String req;
			if ( user.getClasse() != null) {
				req = "INSERT INTO user (login,email,password,idEtablissement,role,nom,prenom,matiere,id_classe) VALUES (?,?,?,?,?,?,?,?,?)";
			}else {
				req = "INSERT INTO user (login,email,password,idEtablissement,role,nom,prenom,matiere) VALUES (?,?,?,?,?,?,?,?)";
			}
			
			PreparedStatement  stm =  cnx.prepareStatement(req ,Statement.RETURN_GENERATED_KEYS);

			stm.setString(1, user.getLogin() );
			stm.setString(2, user.getAdresse() );
			stm.setString(3, user.getPassword() );
			stm.setInt(4,user.getEtablissement().getIdEtablissement());
			stm.setString(5,user.getRole());
			stm.setString(6, user.getNom() );
			stm.setString(7, user.getPrenom() );
			if ( user.getClasse() != null) {
			stm.setInt(9, user.getClasse().getIdClasse() );
			}
	
	
			stm.setInt(8, user.getMatiere().getIdMatiere() );
			int lastid = stm.executeUpdate();

			ResultSet generatedKeys = stm.getGeneratedKeys() ;
			while(generatedKeys.next())
			{
			lastid = generatedKeys.getInt(1);
			}
			return lastid ;



		}

		@Override
		public int delete(int id) throws SQLException {
			Connection cnx =  Connect.getInstance().getConnection() ;

			String req = "DELETE FROM `user` WHERE id = ?";

			PreparedStatement  stm =  cnx.prepareStatement(req);

			stm.setInt(1, id );

			int nbligne = stm.executeUpdate();

			return nbligne;
		}

		
		
		
        public ObservableList<User> findAll() throws ClassNotFoundException, SQLException
        {
       	 ObservableList<User> UserList = FXCollections.observableArrayList();

            // constitution d'une commande bas�e sur une requ�te SQL 
            // en vue d'�tre ex�cut�e sur une connexion donn�e     
            String req = "select  user.id, login, email, password, matiere, role, nom,"
            		+ " prenom, idEtablissement, libelle from user , etablissement WHERE user.idEtablissement = etablissement.id" ;

            Connection cnx = Connect.getInstance().getConnection() ;
          int id ;
          String login;
          String adresseMail;
          String password;
          String  role;
          String nom ;
          String prenom ;
            PreparedStatement pst = cnx.prepareStatement(req);


            ResultSet jeu = pst.executeQuery();
            while(jeu.next()){
           	 id = jeu.getInt("id");
           	 login = jeu.getString("login");
           	 adresseMail =jeu.getString("email");
           	 password = jeu.getString("password");
           	 role = jeu.getString("role");
           	 nom = jeu.getString("nom");
           	 prenom = jeu.getString("prenom");
           	 
           int idEtablissement = jeu.getInt("idEtablissement");
           String libelle = jeu.getString("libelle");
           
           	 Etablissement etatb = new Etablissement() ;
           	 etatb.setIdEtablissement(idEtablissement);
           	 etatb.setLibelle(libelle);
           	 
           	 User user = new User();
           	 user.setIdUser(id);
           	 user.setLogin(login);
           	 user.setEtablissement(etatb);
           	 user.setAdresse(adresseMail);
           	 user.setPassword(password);
           	 user.setNom(nom);
           	 user.setPrenom(prenom);
        	 user.setRole(role);
           	 UserList.add(user);

            }
            jeu.close();
            pst.close();
            cnx.close();
            return UserList;
        }
        
        
        
        public void ModifEuser (User user) throws SQLException, ClassNotFoundException {
        	
        	//connexion
			Connection cnx = Connect.getInstance().getConnection();
			
			//Cr�ation de la requete modification donn�es user
			String requeteSQL = "UPDATE `user` SET `login`=?, `email`=?,`password`=?"
					+ ",`role`=?,`nom`=?,`prenom`=?,`matiere`=? WHERE id =?";
			
			//preparer la requ�te
			PreparedStatement pst = cnx.prepareStatement(requeteSQL);
			
			// renvoyer et verifier les donn�es de la requ�te
    		pst.setString(1, user.getLogin());
    		pst.setString(2, user.getAdresse());
    		pst.setString(3, user.getPassword());
    		pst.setString(4, user.getRole());
    		pst.setString(5, user.getNom());
    		pst.setString(6, user.getPrenom());
    		pst.setInt(7, user.getMatiere().getIdMatiere());
    		pst.setInt(8, user.getIdUser());
    		
    		int nbligne = pst.executeUpdate();
			
			

        }
        


		public static User UserVerif(String login, String mdp) throws SQLException {

			Connection cnx = Connect.getInstance().getConnection();
			String req = "SELECT id, role, email, password FROM User WHERE login=? AND password=? ";
			PreparedStatement pst = cnx.prepareStatement(req);
			pst.setString(1, login);
		    pst.setString(2, mdp);
		    User user = null;
			ResultSet userInformation = pst.executeQuery();
			while(userInformation .next()) {

			 String role = userInformation.getString("role") ;
			 int id =   userInformation.getInt("id") ;
			 String mail = userInformation.getString("email");
			 String password = userInformation.getString("password");
			 user = new User();
             user.setRole(role);
             user.setIdUser(id);
             user.setAdresse(mail);
             user.setPassword(password);


			}

			return user;
		}

		@Override
		public ObservableList<User> findByEleve() throws ClassNotFoundException, SQLException {
			// TODO Auto-generated method stub
			ObservableList<User> UserList = FXCollections.observableArrayList();
	         String str = "[\"ROLE_ELEVE\"]";
	        String req = "SELECT `id`, `idEtablissement`, `login`, `email`, `password`, `role`, `nom`, `prenom` FROM `user` , classe WHERE user.role = ?" ;
	        Connection cnx = Connect.getInstance().getConnection() ;
	      int idUser ;
	      String login;
	      String adresseMail;
	      String password;
	      String role;
	      String nom;
	      String prenom;
	    
	    
	      
	        PreparedStatement pst = cnx.prepareStatement(req);
	        pst.setString(1, str);

	        ResultSet jeu = pst.executeQuery();
	        while(jeu.next()){
	        	 idUser = jeu.getInt("id");
	           	 login = jeu.getString("login");
	           	 adresseMail =jeu.getString("email");
	           	 password = jeu.getString("password");
	           	 role = jeu.getString("role");
	           	 nom = jeu.getString("nom");
	           	 prenom = jeu.getString("prenom");
	       	 
	       	 User user = new User();
	       	 user.setIdUser(idUser);
	       	 user.setLogin(login);
	       	 user.setAdresse(adresseMail);
	       	 user.setPassword(password);
	       	 user.setRole(role);
	       	 user.setNom(nom);
	       	 user.setPrenom(prenom);;
	      
	      
	       	 UserList.add(user);
	       	 
	        }
	        jeu.close();
	        pst.close();
	        return UserList;
		}

		
		@Override
		public ObservableList<User> findBy(int id) throws ClassNotFoundException, SQLException {
			// TODO Auto-generated method stub
			ObservableList<User> UserList = FXCollections.observableArrayList();
	         
	        String req = "SELECT DISTINCT `id`, `idEtablissement`, `login`, `email`, `password`, `role`, `nom`, `prenom` FROM `user` , classe WHERE user.id_classe = classe.id_classe and user.id_classe = ?" ;
	        Connection cnx = Connect.getInstance().getConnection() ;
	      int idUser ;
	      String login;
	      String adresseMail;
	      String password;
	      String role;
	      String nom;
	      String prenom;
	    
	    
	      
	        PreparedStatement pst = cnx.prepareStatement(req);
	        pst.setInt(1, id);

	        ResultSet jeu = pst.executeQuery();
	        while(jeu.next()){
	        	 idUser = jeu.getInt("id");
	           	 login = jeu.getString("login");
	           	 adresseMail =jeu.getString("email");
	           	 password = jeu.getString("password");
	           	 role = jeu.getString("role");
	           	 nom = jeu.getString("nom");
	           	 prenom = jeu.getString("prenom");
	       	 
	       	 User user = new User();
	       	 user.setIdUser(idUser);
	       	 user.setLogin(login);
	       	 user.setAdresse(adresseMail);
	       	 user.setPassword(password);
	       	 user.setRole(role);
	       	 user.setNom(nom);
	       	 user.setPrenom(prenom);;
	      
	      
	       	 UserList.add(user);
	       	 
	        }
	        jeu.close();
	        pst.close();
	        cnx.close();
	        return UserList;
		}

	
}
