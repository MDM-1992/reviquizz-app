package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import controller.ListeClasseController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import metier.Classe;
import metier.User;


/**
 * Author Anaya Kassamaly
 */
public class ClasseDAO implements GestionInterface<Classe>{
	
	@Override
	public int insert(Classe classe) throws SQLException {

		Connection cnx = Connect.getInstance().getConnection() ;
		


		String req = "INSERT INTO classe (id_classe, libelle) VALUES (?,?)";

		PreparedStatement  stm =  cnx.prepareStatement(req);

		stm.setInt(1, classe.getIdClasse());
		stm.setString(2, classe.getLibelle());
	
	
		int nbligne = stm.executeUpdate();
			
		return nbligne;	
	}

	public static void insertClasse(Classe classe) throws SQLException, ClassNotFoundException {
		// Je me connecte
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te inserer new classe
		String requeteSQL = "INSERT INTO `classe`(`libelle`) "
				+ "VALUES (?)";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setString(1, classe.getLibelle());


		int nbligne = pst.executeUpdate();

	}
	

	public  void ModifClasse(Classe classe) throws SQLException, ClassNotFoundException {
		// Je me connecte
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te inserer new pers
		String requeteSQL = "UPDATE classe SET libelle= ? WHERE id_classe =?";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setString(1, classe.getLibelle());
		int nbligne = pst.executeUpdate();

	
		
	}


	//requ�te pour r�cup�rer seulement le libelle etablissement pour la comboBox de UserEditDialog
	public ObservableList<Classe> findLibelleClasse() throws ClassNotFoundException, SQLException {
		ObservableList<Classe> listeE = FXCollections.observableArrayList();
		Connection cnx = Connect.getInstance().getConnection();
		String req = "select id_classe , libelle from classe";
		PreparedStatement pst = cnx.prepareStatement(req);
		ResultSet jeu = pst.executeQuery();
		while(jeu.next()){
		Classe classe = new Classe();
		classe.setLibelle(jeu.getString("libelle"));
		classe.setIdClasse(jeu.getInt("id_classe"));
		listeE.add(classe);
		}
		
        jeu.close();
        pst.close();
        cnx.close();
		return listeE;
		}


	@Override
	public ObservableList<Classe> findAll() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		String req = "select * from Classe";
		Connection co = Connect.getInstance().getConnection();
		int id;
		String Libelle;
		ObservableList<Classe> listeClasse = FXCollections.observableArrayList();
		PreparedStatement pst = co.prepareStatement(req);
		
		ResultSet table = pst.executeQuery();
		while(table.next()) {
			id = table.getInt("id_classe");
			Libelle = table.getString("Libelle");

			
			Classe classe = new Classe();
			classe.setIdClasse(id);
			classe.setLibelle(Libelle);

			listeClasse.add(classe);
		}
		table.close();
		pst.close();
		co.close();
		return listeClasse;
	}

	@Override
	public int delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		Connection co = Connect.getInstance().getConnection();

		// Cr�ation de la requ�te supprimer etablissement
		String requeteSQL = "DELETE FROM `classe` WHERE id_classe = ?";

		// pr�parer la requ�te
		PreparedStatement pst = co.prepareStatement(requeteSQL);

		// renvoyer et verifier les donn�es de la requ�te
		pst.setInt(1, id);
		
		
		
		int i = pst.executeUpdate();

		return i;
	}


	@Override
	public ObservableList<Classe> findBy(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<User> findByEleve() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
