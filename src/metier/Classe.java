package metier;



import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author Anaya Kassamaly
 */
public class Classe {
	
private IntegerProperty idClasse;
	
	private StringProperty libelle;
	
	public Classe() {
		this.idClasse = new SimpleIntegerProperty();
		this.libelle = new SimpleStringProperty();
		
	}
	
	
	
	
	

	public IntegerProperty getIdClasseP() {
		return idClasse;
	}

	public void setIdClasseP(IntegerProperty idClasse) {
		this.idClasse = idClasse;
	}

	public StringProperty getLibelleP() {
		return libelle;
	}

	public void setLibelleP(StringProperty libelle) {
		this.libelle = libelle;
	}
	
	
	public int getIdClasse() {
		return idClasse.get();
	}

	public void setIdClasse(int idClasse) {
		this.idClasse.set(idClasse);
	}

	public String getLibelle() {
		return libelle.get();
	}

	public void setLibelle(String libelle) {
		this.libelle.set(libelle);;
	}






	@Override
	public String toString() {
		return "" + libelle.get();
	}
	
	
	

}
