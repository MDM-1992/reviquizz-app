package metier;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Author Thomas Haloche
 */
public class Matiere {

	private IntegerProperty idMatiere;
	private StringProperty libelle;

	public Matiere() {
		this.idMatiere = new SimpleIntegerProperty();
		this.libelle = new SimpleStringProperty();
	}

	//getter setter de Property
	public IntegerProperty getIdMatiereP() {
		return idMatiere;
	}

	public void setIdMatiereP(IntegerProperty idMatiere) {
		this.idMatiere = idMatiere;
	}

	public StringProperty getlibelleP() {
		return libelle;
	}

	public void setlibelleP(StringProperty libelle) {
		this.libelle = libelle;
	}

	//getter setter de base
	public int getIdMatiere() {
		return idMatiere.get();
	}
	public void setIdMatiere(int idMatiere) {
		this.idMatiere.set(idMatiere);
	}

	public void setlibelle(String libelle) {
		this.libelle.set(libelle);
	}
	public String getlibelle() {
		return libelle.get();
	}

	@Override
	public String toString() {
		return  libelle.get() ;
	}
}
