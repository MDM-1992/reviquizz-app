package metier;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Author Anaya Kassamaly
 */
public class Etablissement {
	
	private IntegerProperty idEtablissement;
	private StringProperty libelle;
	private IntegerProperty codePostal;
	private StringProperty adresse;
	private StringProperty ville;
	private User user;
	
	public Etablissement() {
		this.idEtablissement = new SimpleIntegerProperty();
		this.libelle = new SimpleStringProperty();
		this.codePostal = new SimpleIntegerProperty();
		this.adresse = new SimpleStringProperty();
		this.ville = new SimpleStringProperty();
	}
	

	//getter setter de property
	public IntegerProperty getIdEtablissementP() {
		return idEtablissement;
	}

	public void setIdEtablissementP(IntegerProperty idEtablissement) {
		this.idEtablissement = idEtablissement;
	}

	public StringProperty getLibelleP() {
		return libelle;
	}

	public void setLibelleP(StringProperty libelle) {
		this.libelle = libelle;
	}
	

	
	
	public IntegerProperty getCodePostalP() {
		return codePostal;
	}


	public void setCodePostalP(IntegerProperty codePostal) {
		this.codePostal = codePostal;
	}


	public StringProperty getAdresseP() {
		return adresse;
	}


	public void setAdresseP(StringProperty adresse) {
		this.adresse = adresse;
	}


	public StringProperty getVilleP() {
		return ville;
	}


	public void setVilleP(StringProperty ville) {
		this.ville = ville;
	}


	// getter setter de base

	public IntegerProperty getcodePostalP() {
		return codePostal;
	}
	
	public void setcodePostalP(IntegerProperty codePostal) {
		this.codePostal = codePostal;
	}
	
	public StringProperty getadresseP() {
		return adresse;
	}
	
	public void setadresseP(StringProperty adresse) {
		this.adresse = adresse;
	}
	
	public StringProperty getvilleP() {
		return ville;
	}
	
	public void setvilleP(StringProperty ville) {
		this.ville = ville;
	}
	                             // getter setter de base

	public int getIdEtablissement() {
		//Sreturn 7;
		return this.idEtablissement.get() ;
	}

	public void setIdEtablissement(int idEtablissement) {
		this.idEtablissement.set(idEtablissement);
	}

	public String getLibelle() {
		return libelle.get();
	}

	public void setLibelle(String libelle) {
		this.libelle.set(libelle);
	}
	

	public int getCodePostal( ) {
		return codePostal.get();
	}
	public void setcodePostal(int codePostal) {
		this.codePostal.set(codePostal);
	}

	public String getAdresse() {
		return adresse.get();
	}

	public void setAdresse(String adresse) {
		this.adresse.set(adresse);
	}
	public String getVille() {
		return adresse.get();
	}

	public void setVille(String ville) {
		this.ville.set(ville);
	}
	
	

	@Override
	public String toString() {
		return "Etablissement [idEtablissement=" + idEtablissement + ", libelle=" + libelle + ", codePostal="
				+ codePostal + ", adresse=" + adresse + ", ville=" + ville + "]";
	}


	public static Object getSelectionModel() {
		// TODO Auto-generated method stub
		return null;
	}


	public int getId() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public String getadresse() {
		return adresse.get();
	}
	
	public void setadresse(String adresse) {
		this.adresse.set(adresse);
	}
	
	public String getville() {
		return ville.get();
	}
	
	public void setville(String ville) {
		this.ville.set(ville);
	}

	public void setUser(User user) {
		// TODO Auto-generated method stub
		this.user = user ;
	}
}
