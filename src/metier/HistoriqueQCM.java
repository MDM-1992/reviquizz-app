package metier;

import java.util.Date;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Author Jeremy Roche
 */
public class HistoriqueQCM {

	User user;
	Questionnaire questionnaire;
	private IntegerProperty idHisto;
	private ObjectProperty<Date> dateQCM;
	private IntegerProperty resultat;
	
	public HistoriqueQCM() {
		this.idHisto = new SimpleIntegerProperty();
		this.user = new User();
		this.questionnaire = new Questionnaire();
		this.dateQCM = new SimpleObjectProperty<Date>();
		this.resultat = new SimpleIntegerProperty();
	}
	
	//getter setter de classes 
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Questionnaire getQuestionnaire() {
		return questionnaire;
	}
	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}
	
	//getter setter de Property
	public IntegerProperty getIdHistoP() {
		return idHisto;
	}
	public void setIdHistoP(IntegerProperty idHisto) {
		this.idHisto = idHisto;
	}
	public ObjectProperty<Date> getDateQCMP() {
		return dateQCM;
	}
	public void setDateQCMP(ObjectProperty<Date> dateQCM) {
		this.dateQCM = dateQCM;
	}
	public IntegerProperty getResultatP() {
		return resultat;
	}
	public void setResultatP(IntegerProperty resultat) {
		this.resultat = resultat;
	}	
	
	//getter setter de base
	public int getIdHisto() {
		return idHisto.get();
	}
	public void setIdHisto(int idHisto) {
		this.idHisto.set(idHisto);
	}
	public java.sql.Date getDateQCM() {
		return (java.sql.Date) dateQCM.get();
	}
	public void setDateQCM(ObjectProperty<Date> dateQCM) {
		this.dateQCM = dateQCM;
	}
	public int getResultat() {
		return resultat.get();
	}
	public void setResultat(int resultat) {
		this.resultat.set(resultat);
	}
}
