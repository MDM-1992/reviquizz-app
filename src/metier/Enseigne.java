package metier;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Enseigne {
	
	private IntegerProperty id;
	private IntegerProperty id_user;
	ObservableList<Classe> listeclasse  ;
	
	
	
	
	public Enseigne() {
		this.id = new SimpleIntegerProperty();
		this.listeclasse =  FXCollections.observableArrayList();
		this.id_user = new SimpleIntegerProperty();
		

}

	public int getId() {
		return this.id.get();
	}
	public int getIdUser() {
		return this.id_user.get();
	}
	
	
	public void setId(int idUser) {
		this.id.set(idUser);
	}
	
	public void setIdUser(int idUser) {
		this.id_user.set(idUser);
	}

	public IntegerProperty getIdP() {
		return id;
	}

	public void setId(IntegerProperty id) {
		this.id = id;
	}



	

	public ObservableList<Classe> getListeclasse() {
		return listeclasse;
	}

	public void setListeclasse(ObservableList<Classe> listeclasse) {
		this.listeclasse = listeclasse;
	}

	public IntegerProperty getId_user() {
		return id_user;
	}

	public void setId_user(IntegerProperty id_user) {
		this.id_user = id_user;
	}
}
