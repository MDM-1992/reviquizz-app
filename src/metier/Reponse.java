package metier;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Reponse {

	Question question;
	private IntegerProperty idReponse;
	private BooleanProperty valeur;
	private StringProperty libelle;
	
	public Reponse() {
		this.idReponse = new SimpleIntegerProperty();
		this.question = new Question();
		this.valeur = new SimpleBooleanProperty();
		this.libelle = new SimpleStringProperty();
	}
	
	//Getter Setter de classes 
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	} 
	
	//getter setter de Property
	public IntegerProperty getIdReponseP() {
		return idReponse;
	}
	public void setIdReponseP(IntegerProperty idReponse) {
		this.idReponse = idReponse;
	}
	public BooleanProperty getValeurP() {
		return valeur;
	}
	public void setValeurP(BooleanProperty valeur) {
		this.valeur = valeur;
	}
	public StringProperty getLibelleP() {
		return libelle;
	}
	public void setLibelleP(StringProperty libelle) {
		this.libelle = libelle;
	}
	
	//getter setter de base
	public int getIdReponse() {
		return idReponse.get();
	}
	public void setIdReponse(int idReponse) {
		this.idReponse.set(idReponse);
	} 
	public boolean getValeur() {
		return valeur.get();
	}
	public void setValeur(boolean valeur) {
		this.valeur.set(valeur);
	}
	public String getLibelle() {
		return libelle.get();
	}
	public void setLibelle(String libelle) {
		this.libelle.set(libelle);
	}
}
