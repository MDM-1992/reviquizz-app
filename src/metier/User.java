package metier;

import org.apache.commons.codec.digest.DigestUtils;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Author Marc Missler
 */
public class User {

	Classe classe;
	Etablissement etablissement;
	private IntegerProperty idUser;
	private StringProperty login;
	private StringProperty adresseMail;
	private String password;
	private StringProperty role;
	private StringProperty nom;
	private StringProperty prenom;
	private Matiere matiere;

	public User() {
		this.idUser = new SimpleIntegerProperty();
		this.role = new SimpleStringProperty();
		this.login = new SimpleStringProperty();
		this.adresseMail = new SimpleStringProperty();
		this.nom = new SimpleStringProperty();
		this.prenom = new SimpleStringProperty();
		this.password = new String();
		this.matiere = new Matiere();
		this.etablissement = new Etablissement();
		
	}

	//getter setter de classes
	public Etablissement getEtablissement() {
		return etablissement;
	}
	public void setEtablissement(Etablissement etablissement) {
		this.etablissement = etablissement;
	}
	public Classe getClasse() {
		return classe;
	}
	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	//getter setter de Property
	public IntegerProperty getIdUserP() {
		return idUser;
	}
	public void setIdUserP(IntegerProperty idUser) {
		this.idUser = idUser;
	}
	public StringProperty getLoginP() {
		return login;
	}
	public void setLoginP(StringProperty login) {
		this.login = login;
	}
	public StringProperty getAdresseP() {
		return adresseMail;
	}
	public void setAdresseP(StringProperty adresse) {
		this.adresseMail = adresse;
	}

	/*
	 * public StringProperty getPasswordP() { return password; } public void
	 * setPasswordP(StringProperty password) { this.password = password; }
	 */
	public StringProperty getRoleP() {
		return role;
	}
	public void setRoleP(StringProperty role) {
		this.role = role;
	}
	public StringProperty getNomP() {
		return nom;
	}

	public void setNom(StringProperty nom) {
		this.nom = nom;
	}
	
	public Matiere getMatiereP() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	//getter setter de base
	
	public int getIdUser() {
		return idUser.get();
	}
	public void setIdUser(int idUser) {
		this.idUser.set(idUser);
	}
	public String getLogin() {
		return login.get();
	}
	public void setLogin(String login) {
		this.login.set(login);
	}
	public String getAdresse() {
		return adresseMail.get();
	}
	public void setAdresse(String adresse) {
		this.adresseMail.set(adresse);
	}

	/*
	 * public void setPassword(String password) { this.password.set(password);
	 
	}*/
	public String getRole() {
		return role.get();
	}	
	public void setRole(String role) {
		this.role.set(role);
	}

	public String getNom() {
		return nom.get();
	}

	public void setNom(String nom) {
		this.nom.set(nom);
	}


	public String getPrenom() {
		return prenom.get();
	}

	public void setPrenom(String prenom) {
		this.prenom.set(prenom);
	}
	
	public Matiere getMatiere() {
		return matiere;
	}

	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


// permet d'afficher les infos User
	@Override
	public String toString() {
		return "User [etablissement=" + etablissement + ", idUser=" + idUser + ", login=" + login + ", adresseMail="
				+ adresseMail + ", password=" + password + ", role=" + role + ", nom=" + nom + ", prenom=" + prenom
				+ ", matiere=" + matiere + "]";
	}

	public StringProperty getPrenomP() {
		// TODO Auto-generated method stub
		return this.prenom;
	}

	public String getEmail() {
		// TODO Auto-generated method stub
		return null;
	}






}
