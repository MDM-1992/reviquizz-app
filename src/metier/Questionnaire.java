package metier;

import java.util.ArrayList;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Questionnaire {
	Matiere matiere; 									//Matiere r�f�renc�e du quizz
	Classe classe; 										//Classe du quizz
	private IntegerProperty idQuestionnaire;			//Id auto increment de Questionnaire
	private StringProperty libelle; 					//Libelle du quizz
	private ArrayList<Question> tableQuestion; 			//permet de stocker toutes les questions contenues dans le quizz pour la gestion de fenetre
	private IntegerProperty NbQuestions;
	
	public Questionnaire() {
		this.idQuestionnaire = new SimpleIntegerProperty();
		this.matiere = new Matiere();
		this.libelle = new SimpleStringProperty();
		this.NbQuestions = new SimpleIntegerProperty();
		this.tableQuestion = new ArrayList<Question>();
	}
	
	//getter setter de classes
	public Matiere getMatiere() {
		return matiere;
	}
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}
	public Classe getClasse() {
		return classe;
	}
	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	//getter setter de Property
	public IntegerProperty getIdQuestionnaireP() {
		return idQuestionnaire;
	}
	public void setIdQuestionnaireP(IntegerProperty idQuestionnaire) {
		this.idQuestionnaire = idQuestionnaire;
	}
	public StringProperty getLibelleP() {
		return libelle;
	}
	public void setLibelleP(StringProperty libelle) {
		this.libelle = libelle;
	}
	public IntegerProperty getNbQuestionsP() {
		return NbQuestions;
	}
	public void setNbQuestionsP(IntegerProperty nbquestions) {
		this.NbQuestions = nbquestions;
	}
	
	//getter setter de base
	public int getIdQuestionnaire() {
		return idQuestionnaire.get();
	}
	public void setIdQuestionnaire(int idQuestionnaire) {
		this.idQuestionnaire.set(idQuestionnaire);
	}
	public String getLibelle() {
		return libelle.get();
	}
	public void setLibelle(String libelle) {
		this.libelle.set(libelle);
	}
	public int getNbQuestions() {
		return NbQuestions.get();
	}
	public void setNbQuestions(int nbquestions) {
		this.NbQuestions.set(nbquestions);
	}
	public Question getList(int placeTable){
		return this.tableQuestion.get(placeTable);
	}
	public void setList(int placeTable,Question question) {
		this.tableQuestion.set(placeTable, question);
	}
	
	public void initTable() {
		for(int i=0;i<20;i++) {
			this.tableQuestion.add(i,null);
		}
	}
}
