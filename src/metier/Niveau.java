package metier;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Niveau {
	private IntegerProperty idNiveau;
	private StringProperty libelle; 
	
	public Niveau() {
		this.idNiveau = new SimpleIntegerProperty();
		this.libelle = new SimpleStringProperty();
	}
		
		//getter setter de Property
		public IntegerProperty getIdNiveauP() {
			return idNiveau;
		}
		public void setIdNiveauP(IntegerProperty idNiveau) {
			this.idNiveau = idNiveau;
		}
		public StringProperty getLibelleP() {
			return libelle;
		}
		public void setLibelleP(StringProperty libelle) {
			this.libelle = libelle;
		}
		
		//getter setter de base
		public int getIdNiveau() {
			return idNiveau.get();
		}
		public void setIdNiveau(int idNiveau) {
			this.idNiveau.set(idNiveau);
		}
		public String getLibelle() {
			return libelle.get();
		}
		public void setLibelle(String libelle) {
			this.libelle.set(libelle);
		}
}
