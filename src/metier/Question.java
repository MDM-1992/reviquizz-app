package metier;

import java.util.ArrayList;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Question {
	
	Questionnaire questionnaire;
	private IntegerProperty idQuestion;
	private StringProperty libelle;
	private IntegerProperty numQuestion;
	private ArrayList<Reponse> tableReponse;
	
	public Question() {
		this.idQuestion = new SimpleIntegerProperty();
		this.questionnaire = new Questionnaire();
		this.libelle = new SimpleStringProperty(); 
		this.numQuestion = new SimpleIntegerProperty();
		this.tableReponse = new ArrayList<Reponse>();
	}
	
	//getter setter de classes	
	public Questionnaire getQuestionnaire() {
		return questionnaire;
	}
	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}
	
	//getter setter de Property
	public IntegerProperty getIdQuestionP() {
		return idQuestion;
	}
	public void setIdQuestionP(IntegerProperty idQuestion) {
		this.idQuestion = idQuestion;
	}
	public StringProperty getLibelleP() {
		return libelle;
	}
	public void setLibelleP(StringProperty libelle) {
		this.libelle = libelle;
	}
	public IntegerProperty getNumQuestionP() {
		return numQuestion;
	}
	public void setNumQuestionP(IntegerProperty numQuestion) {
		this.numQuestion = numQuestion;
	}
	
	//getter setter de base 
	public int getIdQuestion() {
		return idQuestion.get();
	}
	public void setIdQuestion(int idQuestion) {
		this.idQuestion.set(idQuestion);
	}
	public String getLibelle(){
		return libelle.get();
	}
	public void setLibelle(String libelle) {
		this.libelle.set(libelle);
	}
	public int getNumQuestion(){
		return numQuestion.get();
	}
	public void setNumQuestion(int numQuestion) {
		this.numQuestion.set(numQuestion);
	}
	public Reponse getList(int placeTable){
		return this.tableReponse.get(placeTable);
	}
	public void setList(int placeTable,Reponse reponse) {
		this.tableReponse.set(placeTable,reponse);
	}
	
	
	public void initTable() {
		this.tableReponse.add(0,null);
		this.tableReponse.add(1,null);
		this.tableReponse.add(2,null);
		this.tableReponse.add(3,null);
	}
	
}


















